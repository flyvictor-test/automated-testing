Victor test automation framework
================================

Setting up a local environment using vagrant
-----------------------------
Assumes:
* mac/linux, the ./vagrant shortcut file doesn't work on Windows, so instead cd into selenium grid and use the vagrant command
* a working vagrant 1.6.3 environment, with virtualbox. Instructions here if nec (https://bitbucket.org/flyvictor/automated-testing-vagrant) step 1 only.

Having cloned the acceptance-testing repo:
* git submodule init
* git submodule update
* ./vagrant up
* ./vagrant bi
* ./vagrant ssh
* bundle exec rake features profile=api env=staging