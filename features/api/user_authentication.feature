@api @user_service @authentication
Feature: User Authentication
  As a user
  I want to authenticate a client
  So that I can access Fly Victor

  @complete
  Scenario: Authenticate using valid credentials
    When a client authenticates using valid credentials
    Then the response code should be 201
    And the response should contain an authentication token

  @complete
  Scenario: Cannot authenticate using invalid credentials
    When a client authenticates using invalid credentials
    Then the response code should be 403
    And the response should not contain an authentication token