When(/^I attempt to access restricted content$/) do
  @denied_access_failures = @my_accounts_page.check_denied_access
end

Then(/^I should be denied access to the restricted content$/) do
  expect(@denied_access_failures).to eql([]), @denied_access_failures.join
end