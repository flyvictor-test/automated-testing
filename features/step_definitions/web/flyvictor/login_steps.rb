Given(/^I am (?:an|a) (unregistered user|registered user|member|delegate|travel agent|administrator)$/) do |role|
  @flyvictor_user = @data_manager.user_by_role(role)
end

Given(/^I am a registered user who has forgotten my password$/) do
  @flyvictor_user = @data_manager.user_by_role("reset password")
end

Given(/^I am signed in as (?:a|an) (registered user|member|administrator)$/) do |role|
  @home_page.navigate_to unless @header_bar.is_open?
  unless @header_bar.is_user_authenticated?
    @flyvictor_user = @data_manager.user_by_role(role)
    @login_dialog.do_login(@flyvictor_user)
    expect(@upcoming_flights_page.wait_until_opened?).to be true
    expect(@header_bar.wait_until_user_authenticated?).to be true
  end
end

Given(/^I am signed in as that (?:member|registered user)$/) do
  @home_page.navigate_to unless @header_bar.is_open?
  @login_dialog.do_login(@flyvictor_user) unless @header_bar.is_user_authenticated?
  expect(@header_bar.wait_until_user_authenticated?).to be true
end

Given(/^I am (?:an unauthenticated user|signed out)$/) do
  @home_page.navigate_to unless @header_bar.is_open?
  @header_bar.select_sign_out_link if @header_bar.is_user_authenticated?
end

When(/^I choose to sign in$/) do
  @home_page.navigate_to unless @header_bar.is_open?
  @login_dialog.click_login_link
end

When(/^I sign in using (?:valid|unverified|updated) credentials$/) do
  # Assign a user if one has not already been assigned.
  @flyvictor_user ||= @data_manager.user_by_role("member")
  @login_dialog.enter_credentials(@flyvictor_user.email, @flyvictor_user.password)
  @login_dialog.click_sign_in_link
end

When(/^I sign in using credentials that (omit|contain) (?:the|an invalid) (email address|password)$/) do |action, field|
  action.eql?("omit") ? @login_dialog.invalidate_credentials(field, true) : @login_dialog.invalidate_credentials(field)
  @login_dialog.click_sign_in_link
end

When(/^I sign in without entering credentials$/) do
  @login_dialog.click_sign_in_link
end

When(/^I choose to become a member$/) do
  @login_dialog.click_become_a_member_link
end

When(/^I sign out$/) do
  @home_page.navigate_to unless @header_bar.is_open?
  @header_bar.select_sign_out_link if @header_bar.is_user_authenticated?
end

Then(/^I should be instructed to sign in or register$/) do
  @is_signed_in = false
  expect(@login_dialog.wait_until_opened?).to be true
end

Then(/^I should see placeholders for sign in details that default to empty$/) do
  expect(@login_dialog.check_credentials_placeholders).to eql([])
end

Then(/^I should be provided with an option to become a member$/) do
  expect(@login_dialog.become_a_member_link_exists?).to be true
end

Then(/^I should see a notification that the credentials are invalid$/) do
  @login_dialog.check_credentials_error_text
  expect{}.not_to raise_error
end

Then(/^I should see notification that the email address is invalid$/) do
  @login_dialog.check_reset_error_text
  expect{}.not_to raise_error
end

Then(/^(?:I|they) should be signed in to (?:my|their) account$/) do
  expect(@header_bar.wait_until_user_authenticated?).to be true
end

Then(/^I should not be signed in$/) do
  expect(@header_bar.wait_until_user_signed_out?).to be true
end

Then(/^I should see my member name$/) do
  expect(@header_bar.member_name_text).not_to be_empty
end
