Given(/^I have chosen to register$/) do
  @home_page.navigate_to unless @header_bar.is_open?
  @join_page.click_join_link
  expect(@join_page.wait_until_opened?).to be true
end

# When(/^I enter registration details for an existing member$/) do
#   user = @data_manager.user_by_role("member")
#   @join_page.enter_personal_details(user)
# end

When(/^I enter( mandatory)? registration details$/) do |mandatory|
  mandatory ? @join_page.enter_personal_details(@flyvictor_user, true) : @join_page.enter_personal_details(@flyvictor_user)
end

When(/^I enter a password$/) do
  @join_page.enter_password("anon1984")
end

When(/^I enter the password "(.*?)"$/) do |password|
  @join_page.enter_password(password)
end

When(/^I confirm a mismatching password$/) do
  @join_page.confirm_password("anon1985")
end

When(/^I accept Victor's terms and conditions$/) do
  @join_page.click_terms_and_conditions_checkbox
end

When(/^I proceed to create an account(?:.*?)$/) do
  @join_page.click_create_account_button
end

When(/^(?:I verify my|the delegate verifies their) email account$/) do
  @join_page.verify_email_account(@verification_email_message)
end

When(/^they set up their password$/) do
  @join_page.do_setup_password
end

Then(/^I should be taken to create an account$/) do
  expect(@join_page.wait_until_opened?).to be true
end

Then(/^I should see a notification that the member is already registered$/) do
  @join_page.check_already_registered_email_error_text
  expect{}.not_to raise_error
end

Then(/^I should see a notification that there are an invalid number of password characters$/) do
  @join_page.check_invalid_number_of_password_characters_error_text
  expect{}.not_to raise_error
end

Then(/^I should see a notification that the passwords do not match$/) do
  @join_page.check_unmatched_passwords_error_text
  expect{}.not_to raise_error
end

Then(/^I should be instructed to verify my email account to complete registration$/) do
  failures = @join_page.check_verification_message
  expect(failures).to eql([]), failures.join
end

Then(/^(I|they) should receive an email confirming (?:my|their) membership$/) do |person|
  user = person.eql?("I") ? @flyvictor_user : @delegate_user
  confirmation_email_message = @join_page.read_confirmation_email(user)
  expect(@join_page.check_confirmation_email_content(confirmation_email_message)).to eql([])
end