#
# Used for defining shared steps.
#
Then(/^I should be presented with Victor's contractual agreement$/) do
  case @page_context.current_page
  when "home"
    expect(@home_page.is_terms_and_conditions_displayed?).to be true
  when "join"
    expect(@join_page.is_victor_terms_and_conditions_displayed?).to be true
  end
end