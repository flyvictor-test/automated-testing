#
# Used for defining hooks.
#
Before do
  @page_context ||= PageContext.new(@runtime, @driver)

  # FlyVictor pages.
  @home_page ||= FlyVictor::HomePage.new(@runtime, @locator_registry, @driver, @data_manager)
  @header_bar ||= FlyVictor::HeaderBar.new(@runtime, @locator_registry, @driver, @data_manager)
  @join_page ||= FlyVictor::JoinPage.new(@runtime, @locator_registry, @driver, @data_manager)
  @login_dialog ||= FlyVictor::LoginDialog.new(@runtime, @locator_registry, @driver, @data_manager)
  @my_accounts_page ||= FlyVictor::MyAccountsPage.new(@runtime, @locator_registry, @driver)
  @upcoming_flights_page ||= FlyVictor::UpcomingFlightsPage.new(@runtime, @locator_registry, @driver, @data_manager)
end

#
# After hooks run in the opposite order of which they are registered.
#

After do
  @driver.manage.delete_all_cookies
end

After do
  @home_page.navigate_to unless @header_bar.is_open?
  @header_bar.select_sign_out_link if @header_bar.is_user_authenticated?
end

After do
  @login_dialog.click_close_button if @login_dialog.is_open?
end

After do |scenario|
  if scenario.failed?
    @driver.save_screenshot("screenshots/#{scenario.__id__}.png")
    embed("screenshots/#{scenario.__id__}.png", "image/png", "Screenshot")
  end
end
