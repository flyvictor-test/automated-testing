#
# Used for defining hooks.
#
Before('@api') do |scenario|
  puts "Scenario: #{scenario.name} - started at #{Time.now.strftime("%d/%m/%Y %H:%M:%S")}"
end

Before do
  @authenticate_user = Api::UserService::UserAuthentication.new(@runtime)
end

After('@api') do
  @authenticate_user.delete_auth_token(@user_token) if !@user_token.nil? && !@user_token.eql?("invalid_token")
end
