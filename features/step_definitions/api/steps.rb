#
# Used for defining shared steps.
#
Then(/^the response code should be (.*?)$/) do |response_code|
  expect(@response.code).to be(response_code.to_i)
end