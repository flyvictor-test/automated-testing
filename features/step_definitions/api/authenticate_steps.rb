Given(/^(?:a|the) client (is|is not|has|has not) authenticated$/) do |authenticate|
  @user_token = "" if authenticate.include?("not")
end

When(/^(?:a|the) client authenticates using (valid|invalid) credentials$/) do |status|
  @authenticate_user.create_auth_token(@member_user.email, @member_user.password)
  @user_id = @authenticate_user.read_user_id
  @user_token = @authenticate_user.read_auth_token

  email = status.eql?("invalid") ? "non-existent" : @member_user.email
  @response = @authenticate_user.create_auth_token(email, @member_user.password)
end

Then(/^the response should( not)? contain an authentication token$/) do |condition|
  user_token = @authenticate_user.read_auth_token
  expect(user_token.length > 0).to be condition.nil?
end