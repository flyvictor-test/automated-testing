# Rubygems and Bundler
require 'rubygems'
require 'bundler/setup'

# Gems
require 'cucumber'
require 'selenium-webdriver'
require 'rspec/expectations'
require 'os'
require 'pathname'
require 'xmlsimple'
require 'securerandom'
require 'gmail'
require 'net/pop'
require 'active_support/all'
require 'httparty'
require 'nokogiri'
require 'stripe'
require 'require_all'
require 'certified'

require_all 'lib'

shared_driver = nil
environment = Environment.new
data_manager = DataManager.new
locator_registry = environment.has_property?(WebDriver::Configuration::Keys::LOCATOR_PROFILE) ?
    LocatorRegistry.new.use_profile(environment.property(WebDriver::Configuration::Keys::LOCATOR_PROFILE)) :
    LocatorRegistry.new
member_user = data_manager.user_by_role("member")

# Only run the clean screenshots once.
Before('~@api') do
  $clean_screenshots ||= false
  unless $clean_screenshots
    Dir.mkdir("screenshots") unless File.exists?("screenshots")
    Dir.glob("screenshots/*").each { |filename| File.delete(filename) }
    $clean_screenshots = true
  end
end

Before do |scenario|
  @runtime = Runtime.new(scenario, environment)
  @data_manager = data_manager
end

Before('~@api') do
  @locator_registry = locator_registry
  # Only instantiate the webdriver once.
  shared_driver = WebDriver::Provider::WebDriverProvider.new(@runtime).get if shared_driver.nil?
  @driver = shared_driver
end

Before('@api') do
  @member_user = member_user
end

at_exit do
  shared_driver.quit unless shared_driver.nil?
end
