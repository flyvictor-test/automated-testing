@authentication
Feature: Sign in
  As a user
  I want to securely sign in using my account credentials
  So that I can access my restricted content

  @complete
  Scenario: Sign in defaults
    When I choose to sign in
    Then I should see placeholders for sign in details that default to empty

  @complete
  Scenario: Option to become a member
    When I choose to sign in
    Then I should be provided with an option to become a member
    When I choose to become a member
    Then I should be taken to create an account

  @complete
  Scenario: Member signs into their account
    Given I am a member
    When I choose to sign in
    And I sign in using valid credentials
    Then I should be signed in to my account
    And I should see my member name

  @complete
  Scenario: User cannot sign in without verifying their account
    Given I am an unregistered user
    And I have chosen to register
    When I enter registration details
    And I accept Victor's terms and conditions
    And I proceed to create an account
    Then I should be instructed to verify my email account to complete registration
    When I choose to sign in
    And I sign in using unverified credentials
    Then I should see a notification that the credentials are invalid
    And I should not be signed in

  @complete
  Scenario: Cannot sign in with invalid email address
    Given I am a registered user
    When I choose to sign in
    And I sign in using credentials that contain an invalid email address
    Then I should see a notification that the credentials are invalid
    And I should not be signed in

  @complete
  Scenario: Cannot sign in with missing email address
    Given I am a registered user
    When I choose to sign in
    And I sign in using credentials that omit the email address
    Then I should see a notification that the credentials are invalid
    And I should not be signed in

  @complete
  Scenario: Cannot sign in with invalid password
    Given I am a registered user
    When I choose to sign in
    And I sign in using credentials that contain an invalid password
    Then I should see a notification that the credentials are invalid
    And I should not be signed in

  @complete
  Scenario: Cannot sign in with missing password
    Given I am a registered user
    When I choose to sign in
    And I sign in using credentials that omit the password
    Then I should see a notification that the credentials are invalid
    And I should not be signed in

  @complete
  Scenario: Cannot sign in with missing credentials
    Given I am a registered user
    When I choose to sign in
    And I sign in without entering credentials
    Then I should see a notification that the credentials are invalid
    And I should not be signed in

  @complete
  Scenario: Unauthenticated user attempts to access restricted content
    Given I am an unauthenticated user
    When I attempt to access restricted content
    Then I should be denied access to the restricted content
