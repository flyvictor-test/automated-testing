class PageContext
  attr_reader :base_url

  def initialize(runtime, driver)
    @driver = driver
    @base_url = runtime.environment.property("flyvictor-url")
  end

  def current_page
    current_url = @driver.current_url
    if current_url.eql?(base_url)
      return "home"
    elsif current_url.eql?(base_url + "join")
      return "join"
    elsif current_url.include?(base_url + "mv/flights/upcoming")
      return "upcomingflights"
    end
    "unspecified"
  end
end
