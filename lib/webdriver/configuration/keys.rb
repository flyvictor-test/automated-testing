module WebDriver
  module Configuration
    class Keys
      LOCATOR_PROFILE = "locator-profile"

      class Selenium
        GRID_HUB_URL = "grid-hub-url"

        PLATFORM = "platform"
        BROWSER_NAME = "browser-name"
        BROWSER_VERSION = "browser-version"
        BROWSER_SIZE = "browser-size"
        DEVICE_ORIENTATION = "device-orientation"
        DEVICE_TYPE = "device-type"

        CHROME_DRIVER_PATH = "chrome-driver-path"
        CHROME_BINARY = "chrome-binary"
        CHROME_SWITCHES = "chrome-switches"

        FIREFOX_BINARY = "firefox-binary"
        FIREFOX_PREFERENCES = "firefox-preferences"

        IE_DRIVER_PATH = "ie-driver-path"

        PHANTOMJS_BINARY = "phantomjs-binary"
      end
    end
  end
end