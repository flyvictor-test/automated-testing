module WebDriver
  module Configuration
    class RemoteWebDriverConfig < Base
      def initialize(environment)
        super
      end

      def grid_hub_url
        @environment.has_property?(Keys::Selenium::GRID_HUB_URL) ? @environment.property(Keys::Selenium::GRID_HUB_URL) : Constants::DEFAULT_GRID_HUB_URL
      end

      def chrome_capabilities
        capabilities = Selenium::WebDriver::Remote::Capabilities.chrome("chromeOptions" => { "args" => chrome_switches })
        capabilities[:platform] = platform
        capabilities[:version] = browser_version
        capabilities["acceptSslCerts"] = true
        capabilities
      end

      def firefox_capabilities
        capabilities = Selenium::WebDriver::Remote::Capabilities.firefox
        capabilities[:firefox_profile] = firefox_profile
        capabilities[:platform] = platform
        capabilities[:version] = browser_version
        capabilities
      end

      def ie_capabilities
        capabilities = Selenium::WebDriver::Remote::Capabilities.internet_explorer
        capabilities[:platform] = platform
        capabilities[:version] = browser_version
        capabilities["acceptSslCerts"] = true
        capabilities
      end

      def safari_capabilities
        capabilities = Selenium::WebDriver::Remote::Capabilities.safari
        capabilities[:platform] = platform
        capabilities[:version] = browser_version
        capabilities["acceptSslCerts"] = true
        capabilities
      end

      private

      def platform
        @environment.has_property?(Keys::Selenium::PLATFORM) ? @environment.property(Keys::Selenium::PLATFORM) : :any
      end
    end
  end
end
