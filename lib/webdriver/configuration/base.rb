module WebDriver
  module Configuration
    class Base
      def initialize(environment)
        @environment = environment
      end

      def browser_name
        @environment.property(Keys::Selenium::BROWSER_NAME)
      end

      def browser_version
        return @environment.property(Keys::Selenium::BROWSER_VERSION) if @environment.has_property?(Keys::Selenium::BROWSER_VERSION)
        ""
      end

      def chrome_switches
        return @environment.property(Keys::Selenium::CHROME_SWITCHES) if @environment.has_property?(Keys::Selenium::CHROME_SWITCHES)
        []
      end

      def firefox_profile
        profile = Selenium::WebDriver::Firefox::Profile.new
        profile.native_events = false
        profile.secure_ssl = true
        profile.assume_untrusted_certificate_issuer = true
        profile
      end
    end
  end
end