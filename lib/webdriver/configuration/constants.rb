module WebDriver
  module Configuration
    class Constants
      DEFAULT_GRID_HUB_URL = "http://localhost:4444/wd/hub"

      class BrowserName
        CHROME = "chrome"
        FIREFOX = "firefox"
        IE = "ie"
        SAFARI = "safari"
        PHANTOMJS = "phantomjs"
      end

      class PlatformName
        IOS = "ios"
        ANDROID = "android"
        FIREFOXOS = "FirefoxOS"
      end
    end
  end
end