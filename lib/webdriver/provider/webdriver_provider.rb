module WebDriver
  module Provider
    class WebDriverProvider
      def initialize(runtime)
        @config = WebDriver::Configuration::WebDriverConfig.new(runtime.environment)
      end

      def get
        context = SeleniumAwareContext.new(@config)
        context.driver
      end
    end

    class SeleniumAwareContext
      def initialize(config)
        @config = config
      end

      def driver
        browser_name = @config.browser_name
        if browser_name.downcase.eql?(WebDriver::Configuration::Constants::BrowserName::CHROME)
          chrome_driver
        elsif browser_name.downcase.eql?(WebDriver::Configuration::Constants::BrowserName::FIREFOX)
          firefox_driver
        elsif browser_name.downcase.eql?(WebDriver::Configuration::Constants::BrowserName::IE)
          ie_driver
        elsif browser_name.downcase.eql?(WebDriver::Configuration::Constants::BrowserName::SAFARI)
          safari_driver
        elsif browser_name.downcase.eql?(WebDriver::Configuration::Constants::BrowserName::PHANTOMJS)
          phantomjs_driver
        else
          raise(UnsupportedConfigurationError, "The browser: #{browser_name} is not supported.\n")
        end
      end

      private

      def chrome_driver
        Selenium::WebDriver::Chrome.path = @config.chrome_binary
        Selenium::WebDriver::Chrome.driver_path = @config.chrome_driver_path
        Selenium::WebDriver.for(:chrome, :detach => false, :switches => @config.chrome_switches)
      end

      def firefox_driver
        caps = Selenium::WebDriver::Remote::Capabilities.firefox
        caps[:firefox_profile] = @config.firefox_profile
        driver = Selenium::WebDriver.for(:firefox, :desired_capabilities => caps)
        driver.manage.window.maximize
        driver
      end

      def ie_driver
        raise(EnvironmentError, "The driver for Internet Explorer is only supported on machines that are not running Windows.\n") unless ENV["OS"] =~ /Windows/
        Selenium::WebDriver::IE.driver_path = @config.ie_driver_path
        caps = Selenium::WebDriver::Remote::Capabilities.internet_explorer
        driver = Selenium::WebDriver.for(:ie, :desired_capabilities => caps)
        driver.manage.window.maximize
        driver
      end

      def safari_driver
        Selenium::WebDriver.for(:safari)
      end

      def phantomjs_driver
        Selenium::WebDriver::PhantomJS.path = @config.phantomjs_binary
        caps = Selenium::WebDriver::Remote::Capabilities.phantomjs
        #TODO Move to the configuration.
        caps["phantomjs.cli.args"] = ["--ignore-ssl-errors=true", "--load-images=true", "--local-to-remote-url-access=true"]
        caps[:javascript_enabled] = true
        Selenium::WebDriver.for(:phantomjs, :desired_capabilities => caps)
      end
    end
  end
end
