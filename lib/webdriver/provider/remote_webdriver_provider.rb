module WebDriver
  module Provider
    class RemoteWebDriverProvider
      def initialize(runtime)
        @config = WebDriver::Configuration::RemoteWebDriverConfig.new(runtime.environment)
      end

      def get
        context = Remote::SeleniumAwareContext.new(@config)
        context.driver
      end

      module Remote
        class SeleniumAwareContext
          def initialize(config)
            @config = config
          end

          def driver
            browser_name = @config.browser_name
            if browser_name.downcase.eql?(WebDriver::Configuration::Constants::BrowserName::CHROME)
              caps = @config.chrome_capabilities
            elsif browser_name.downcase.eql?(WebDriver::Configuration::Constants::BrowserName::FIREFOX)
              caps = @config.firefox_capabilities
            elsif browser_name.downcase.eql?(WebDriver::Configuration::Constants::BrowserName::IE)
              caps = @config.ie_capabilities
            elsif browser_name.downcase.eql?(WebDriver::Configuration::Constants::BrowserName::SAFARI)
              caps = @config.safari_capabilities
            else
              raise(UnsupportedConfigurationError, "The browser: #{browser_name} is not supported.\n")
            end
            Selenium::WebDriver.for(:remote, :url => @config.grid_hub_url, :desired_capabilities => caps)
          end
        end
      end
    end
  end
end