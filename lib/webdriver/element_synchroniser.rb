class ElementSynchroniser
  def initialize(locator_registry, driver, element)
    @locator_registry, @driver, @element = locator_registry, driver, element
  end

  def wait_until_element_located(locator_key, conditions, options = {})
    wait_until_located(locator_key, conditions, Scope::ELEMENT, options)
  end

  def wait_until_child_located(locator_key, conditions, options = {})
    wait_until_located(locator_key, conditions, Scope::CHILDREN, options)
  end

  def wait_until_descendant_located(locator_key, conditions, options = {})
    wait_until_located(locator_key, conditions, Scope::DESCENDANTS, options)
  end

  def wait_until_ajax_calls_finished(timeout = 20)
    end_time = Time.now + timeout
    until Time.now > end_time
      sleep 2
      break if @driver.execute_script("return jQuery.active == 0;")
    end
  end

  private

  def wait_until_located(locator_key, conditions, scope, options = {})
    begin
      options.reverse_merge! default_options
      locator = @locator_registry.locator(locator_key)
      by = locator.by(scope, options[:tag_description])
      wait = Selenium::WebDriver::Wait.new(:timeout => options[:timeout])
      wait.until {
        begin
          !conditions.matches(@driver, @element.find_elements(by)).nil?
        rescue Selenium::WebDriver::Error::WebDriverError
          false
        end
      }
    rescue LocatorError => e
      raise e
    rescue Selenium::WebDriver::Error::TimeOutError
      false
    end
  end

  def default_options
    {
      :tag_description => TagDescription::UNIVERSAL,
      :timeout => 10
    }
  end
end
