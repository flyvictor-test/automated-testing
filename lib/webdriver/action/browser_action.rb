class BrowserAction
  def initialize(driver)
    @driver = driver
  end

  #
  # Switches to the most recently opened window.
  #
  def switch_to_window
    @driver.switch_to.window @driver.window_handles.last unless @driver.window_handles.empty?
  end

  def window_title
    @driver.title
  end

  def close_window
    @driver.close
    switch_to_window
  end

  def switch_to_frame(frame_id)
    @driver.switch_to.frame frame_id
  end

  def switch_to_default
    @driver.switch_to.default_content
  end

  def accept_alert
    @driver.switch_to.alert.accept unless @driver.capabilities[:browser_name] == 'phantomjs'
  end

  def inject_accept_alert
    @driver.execute_script("window.confirm = function(){return true;}") if @driver.capabilities[:browser_name] == 'phantomjs'
  end

  def click(element)
    @driver.action.click(element).perform
  end

  def double_click(element)
    @driver.action.double_click(element).perform
  end

  def scroll_to(element)
    element.location_once_scrolled_into_view
  end

  def select_by_index(element, index)
    select = Selenium::WebDriver::Support::Select.new(element)
    select.select_by(:index, index)
  end

  def select_by_value(element, value)
    select = Selenium::WebDriver::Support::Select.new(element)
    select.select_by(:value, value)
  end

  def select_by_text(element, text)
    select = Selenium::WebDriver::Support::Select.new(element)
    select.select_by(:text, text)
  end

  def first_selected_option(element)
    select = Selenium::WebDriver::Support::Select.new(element)
    select.first_selected_option
  end

  def first_selected_option_text(element)
    first_selected_option(element).text
  end

  def options(element)
    select = Selenium::WebDriver::Support::Select.new(element)
    select.options
  end

  def options_text(element)
    elements_text = []
    options(element).each { |option| elements_text << option.text }
    elements_text
  end

  def send_keys(*keys)
    @driver.action.send_keys(keys).perform
  end

  def focus(element)
    if element.tag_name.casecmp('input') == 0
      element.send_keys('')
      return
    end
    @driver.action.move_to(element).perform
  end

  def hover_over(element)
    #FIXME Adjust javascript according to whether jQuery is enabled.
    @driver.execute_script("$(arguments[0]).mouseover();", element)
  end

  def navigate_to(url)
    @driver.navigate.to(url)
  end

  def refresh
    @driver.navigate.refresh
  end

  def current_url
    @driver.current_url
  end

  def delete_all_cookies
    @driver.manage.delete_all_cookies
  end
end
