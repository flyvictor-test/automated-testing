require 'yaml'

class LocatorRegistry
  def initialize
    @profile = LocatorConstants::DEFAULT_PROFILE
    @registry = Hash.new
    # Register all of the locators specified in the
    # locator.yml file.
    register_locators
    self
  end

  def use_profile(profile)
    unless @registry.has_key?(profile)
      raise(KeyError, <<-END_OF_ERROR)
Could not find profile: '#{profile}'

Defined profiles in locator registry:
  * #{@registry.keys.sort.join("\n  * ")}
      END_OF_ERROR
    end
    @profile = profile
    self
  end

  def locator(locator_key)
    unless @registry[@profile].has_key?(locator_key)
      raise(KeyError, <<-END_OF_ERROR)
Could not find locator key: '#{locator_key}'

Defined locator keys in locator registry for '#{@profile}' profile:
  * #{@registry[@profile].keys.sort.join("\n  * ")}
      END_OF_ERROR
    end
    @registry[@profile][locator_key]
  end

  private

  def register_locators
    profiles = Hash.new
    flattened? ? profiles[LocatorConstants::DEFAULT_PROFILE] = locator_yml : profiles = locator_yml
    profiles.each { |profile, data|
      @registry[profile] = Hash.new
      register_tag_names(profile, data)
      register_xpath_expressions(profile, data)
      register_css_selectors(profile, data)
      register_names(profile, data)
      register_ids(profile, data)
    }
  end

  def register_tag_names(profile, data)
    # Register the locators specified in the :tag-name section
    # of the locator.yml file.
    if data.has_key?(LocatorConstants::TAG_NAME)
      data[LocatorConstants::TAG_NAME].each do |locator_key, value|
        @registry[profile].merge!(locator_key => TagName.new(value))
      end
    end
  end

  def register_xpath_expressions(profile, data)
    # Register the locators specified in the :xpath section
    # of the locator.yml file.
    if data.has_key?(LocatorConstants::XPATH)
      data[LocatorConstants::XPATH].each do |locator_key, value|
        @registry[profile].merge!(locator_key => XPathExpression.new(value))
      end
    end
  end

  def register_css_selectors(profile, data)
    # Register the locators specified in the :css section
    # of the locator.yml file.
    if data.has_key?(LocatorConstants::CSS)
      data[LocatorConstants::CSS].each do |locator_key, value|
        @registry[profile].merge!(locator_key => CSSSelector.new(value))
      end
    end
  end

  def register_names(profile, data)
    # Register the locators specified in the :name section
    # of the locator.yml file.
    if data.has_key?(LocatorConstants::NAME)
      data[LocatorConstants::NAME].each do |locator_key, value|
        @registry[profile].merge!(locator_key => Name.new(value))
      end
    end
  end

  def register_ids(profile, data)
    # Register the locators specified in the :id section
    # of the locator.yml file.
    if data.has_key?(LocatorConstants::ID)
      data[LocatorConstants::ID].each do |locator_key, value|
        @registry[profile].merge!(locator_key => Id.new(value))
      end
    end
  end

  def locator_yml_defined?
    locator_file && File.exist?(locator_file)
  end

  def locator_yml
    unless locator_yml_defined?
      raise(ProfilesNotDefinedError, "locator.yml was not found.  Current directory is #{Dir.pwd}.\n")
    end

    begin
      @locator_yml = YAML.load_file(locator_file)
    rescue StandardError => e
      raise(YmlLoadError, "locator.yml was found, but could not be parsed.\n")
    end

    if @locator_yml.nil? || !@locator_yml.is_a?(Hash)
      raise(YmlLoadError, "locator.yml was found, but was blank or malformed.\n")
    end

    @locator_yml
  end

  # Locates locator.yml file. The file can end in .yml or .yaml,
  # and be located in the current directory (eg. project root) or
  # in a .config/ or config/ subdirectory of the current directory.
  def locator_file
    @locator_file ||= Dir.glob('{,.config/,config/}locator{.yml,.yaml}').first
  end

  def flattened?
    locator_yml.values[0].values[0].is_a?(String)
  end
end
