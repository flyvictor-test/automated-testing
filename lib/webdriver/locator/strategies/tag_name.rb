class TagName
  attr_reader :value

  def initialize(value)
    @value = value
  end

  def by(scope, tag_description)
    case scope
    when Scope::ELEMENT
      return by_element(tag_description)
    else
      raise(LocatorError, "Unsupported scope: '#{scope}'")
    end
  end

  def by_element(tag_description)
    raise(LocatorError, "The TagName locator strategy does not support use of a tag description to further refine the search.") unless tag_description.eql?(TagDescription::UNIVERSAL)
    { :tag_name => value }
  end
end