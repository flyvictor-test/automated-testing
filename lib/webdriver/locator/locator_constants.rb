class LocatorConstants
  DEFAULT_PROFILE = "default"

  ID = "id"
  NAME = "name"
  CSS = "css"
  XPATH = "xpath"
  TAG_NAME = "tag-name"
end