class Page < WebDriver::UIObject::Base
  def initialize(runtime, locator_registry, driver)
    super
  end

  def action
    BrowserAction.new(@driver)
  end
end
