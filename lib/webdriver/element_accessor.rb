class ElementAccessor
  def initialize(locator_registry, driver, element)
    @locator_registry, @driver, @element = locator_registry, driver, element
  end

  def search_for_element(locator_key, conditions, options = {})
    elements = search_for_elements(locator_key, conditions, options)
    return nil if elements.nil?
    elements.first
  end

  def search_for_child(locator_key, conditions, options = {})
    children = search_for_children(locator_key, conditions, options)
    return nil if children.nil?
    children.first
  end

  def search_for_descendant(locator_key, conditions, options = {})
    descendants = search_for_descendants(locator_key, conditions, options)
    return nil if descendants.nil?
    descendants.first
  end

  def search_for_elements(locator_key, conditions, options = {})
    search_for(locator_key, conditions, Scope::ELEMENT, options)
  end

  def search_for_children(locator_key, conditions, options = {})
    search_for(locator_key, conditions, Scope::CHILDREN, options)
  end

  def search_for_descendants(locator_key, conditions, options = {})
    search_for(locator_key, conditions, Scope::DESCENDANTS, options)
  end

  def store_element_text(locator_key, conditions, options = {})
    elements_text = store_elements_text(locator_key, conditions, options)
    return nil if elements_text.empty?
    elements_text.first
  end

  def store_child_text(locator_key, conditions, options = {})
    childrens_text = store_children_text(locator_key, conditions, options)
    return nil if childrens_text.empty?
    childrens_text.first
  end

  def store_descendant_text(locator_key, conditions, options = {})
    descendants_text = store_descendants_text(locator_key, conditions, options)
    return nil if descendants_text.empty?
    descendants_text.first
  end

  def store_elements_text(locator_key, conditions, options = {})
    store_text(locator_key, conditions, Scope::ELEMENT, options)
  end

  def store_children_text(locator_key, conditions, options = {})
    store_text(locator_key, conditions, Scope::CHILDREN, options)
  end

  def store_descendants_text(locator_key, conditions, options = {})
    store_text(locator_key, conditions, Scope::DESCENDANTS, options)
  end

  private

  def search_for(locator_key, conditions, scope, options = {})
    begin
      options.reverse_merge! default_options
      locator = @locator_registry.locator(locator_key)
      by = locator.by(scope, options[:tag_description])
      wait = Selenium::WebDriver::Wait.new(:timeout => options[:timeout])
      wait.until {
        begin
          conditions.matches(@driver, @element.find_elements(by))
        rescue Selenium::WebDriver::Error::WebDriverError
          nil
        rescue
          nil
        end
      }
    rescue LocatorError => e
      raise e
    rescue Selenium::WebDriver::Error::TimeOutError => e
      return nil if options[:suppress_timeout_error]
      raise(ElementNotFoundError, <<-END_OF_ERROR)
Cannot find the element that matches locator: '#{locator.value}'
and conditions: '#{conditions.to_s}'
      END_OF_ERROR
    end
  end

  def store_text(locator_key, conditions, scope, options = {})
    begin
      texts = []
      options.reverse_merge! default_options
      locator = @locator_registry.locator(locator_key)
      by = locator.by(scope, options[:tag_description])
      wait = Selenium::WebDriver::Wait.new(:timeout => options[:timeout])
      wait.until {
        begin
          conditions.matches(@driver, @element.find_elements(by)).each { |element|
            text = ElementUtils.element_text(@driver, element)
            texts << text unless text.nil?
          }
          texts
        rescue Selenium::WebDriver::Error::WebDriverError
          nil
        rescue
          nil
        end
      }
    rescue LocatorError => e
      raise e
    rescue Selenium::WebDriver::Error::TimeOutError => e
      return [] if options[:suppress_timeout_error]
      raise(ElementNotFoundError, <<-END_OF_ERROR)
Cannot find the element that matches locator: '#{locator.value}'
and conditions: '#{conditions.to_s}'
      END_OF_ERROR
    end
  end

  def default_options
    {
      :tag_description => TagDescription::UNIVERSAL,
      :timeout => 10,
      :suppress_timeout_error => false
    }
  end
end
