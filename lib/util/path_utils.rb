module PathUtils
  extend self

  def url?(path)
    uri = URI.parse(path)
    %w( http https ).include?(uri.scheme)
  rescue URI::BadURIError
    false
  rescue URI::InvalidURIError
    false
  end
end