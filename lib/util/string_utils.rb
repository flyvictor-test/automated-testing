module StringUtils
  extend self

  def squish(text)
    text.strip.gsub(/\s+/, " ")
  end
end
