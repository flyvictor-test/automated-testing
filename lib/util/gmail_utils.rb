module GmailUtils
  extend self

  def unread_emails(email_address, password, filters = {})
    filters.reverse_merge! default_filters
    messages = []
    Gmail.connect(email_address, password) do |gmail|
      end_time = Time.now + filters[:timeout]
      until Time.now > end_time || messages.count > 0
        gmail.inbox.emails(:unread, filters).each { |email|
          message = email.body
          message.parts.each { |part|
            part.decoded
          }
          messages << message if message.include?(filters[:content])
          email.read!
        }
        sleep 1
      end
    end
    messages
  end

  private

  def default_filters
    {
        :after => Time.now - 1.day,
        :timeout => 30,
        :content => ''
    }
  end
end
