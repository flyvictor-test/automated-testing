module FlyVictor
  class HeaderBar < Page
    def initialize(runtime, locator_registry, driver, data_manager)
      super(runtime, locator_registry, driver)
      @environment = runtime.environment
      @logger = runtime.logger
      @data_manager = data_manager
    end

    def select_charters_link
      select_my_account_link(@data_manager.content["myAccountChartersLinkText"])
    end

    def select_flights_link
      select_my_account_link(@data_manager.content["myAccountFlightsLinkText"])
    end

    def select_settings_link
      select_my_account_link(@data_manager.content["myAccountSettingsLinkText"])
    end

    def select_sign_out_link
      select_my_account_link(@data_manager.content["myAccountSignOutLinkText"])
      synchroniser.wait_until_ajax_calls_finished
    end

    def check_header_links
      links_text = accessor.store_descendants_text("header.top-menu", conditions.displayed?, :tag_description => TagDescription::HYPERLINK)
      @data_manager.content["homeHeaderLinks"] - links_text
    end

    def member_name_text
      accessor.store_element_text("header.displayname", conditions.displayed?)
    end

    def is_user_authenticated?
      synchroniser.wait_until_element_located("header.my-account-dropdown", conditions.displayed?, :timeout => 2)
    end

    def wait_until_user_authenticated?
      synchroniser.wait_until_element_located("header.my-account", conditions.displayed?.text_contains?("My Account"), :timeout => 30)
    end

    def wait_until_user_signed_out?
      synchroniser.wait_until_element_located("login.link", conditions.displayed?.enabled?)
    end

    def is_open?
      synchroniser.wait_until_element_located("header.top-menu", conditions.displayed?, :timeout => 1)
    end

    def wait_until_opened?
      synchroniser.wait_until_element_located("header.top-menu", conditions.displayed?)
    end

    private

    def select_my_account_link(text)
      my_account_dropdown = accessor.search_for_element("header.my-account-dropdown", conditions.displayed?.enabled?)
      action.hover_over(my_account_dropdown)
      my_account_item = accessor.search_for_descendant("header.my-account-dropdown-list", conditions.displayed?.enabled?.text_contains?(text, true), :tag_description => TagDescription::HYPERLINK)
      my_account_item.click
    end
  end
end
