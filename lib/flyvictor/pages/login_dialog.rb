module FlyVictor
  class LoginDialog < Page
    def initialize(runtime, locator_registry, driver, data_manager)
      super(runtime, locator_registry, driver)
      @data_manager = data_manager
      @environment = runtime.environment
      @logger = runtime.logger
    end

    def do_login(user)
      click_login_link
      enter_credentials(user.email, user.password)
      click_sign_in_link
    end

    def click_login_link
      login_link = accessor.search_for_element("login.link", conditions.displayed?.enabled?)
      login_link.click
      @logger.debug('Clicked login link.')
      wait_until_opened?
    end

    def enter_credentials(email, password)
      enter_email(email)
      enter_password(password)
    end

    def invalidate_credentials(field, omit = false)
      enter_email("flyvictortesting@gmail.com") unless field.eql?("email address")
      enter_password("anon1984") unless field.include?("password")

      # Input text into the invalid field last, unless omit has been specified,
      # so that form level validation is exercised.
      case field
      when "email address"
        enter_email("@flyictor.com") unless omit
      when "password"
        enter_password("anon1985") unless omit
      end
    end

    def enter_email(email)
      login_email.send_keys email
    end

    def enter_password(password)
      login_password.send_keys password
    end

    def click_sign_in_link
      sign_in_link = accessor.search_for_element("login.sign-in-link", conditions.displayed?.enabled?)
      sign_in_link.click
      @logger.debug('Clicked sign in link.')
      synchroniser.wait_until_ajax_calls_finished
    end

    def click_become_a_member_link
      become_a_member_link = accessor.search_for_element("login.become-a-member-link", conditions.displayed?.enabled?)
      become_a_member_link.click
    end

    def click_close_button
      close_button = accessor.search_for_element("login.close-button", conditions.displayed?.enabled?)
      close_button.click
    end

    def check_credentials_placeholders
      failures = []
      email_address_placeholder_content = @data_manager.content["loginEmailAddressPlaceholder"]
      password_placeholder_content = @data_manager.content["loginPasswordPlaceholder"]
      failures << "The email address placeholder: #{email_placeholder} is not: #{email_address_placeholder_content}." unless email_placeholder.eql?(email_address_placeholder_content)
      failures << "The email address text is not empty." unless email_text.eql?('')
      failures << "The password placeholder: #{password_placeholder} is not: #{password_placeholder_content}." unless password_placeholder.eql?(password_placeholder_content)
      failures << "The password text is not empty." unless password_text.eql?('')
      failures
    end

    def check_credentials_error_text
      error_content = @data_manager.content["loginInvalidCredentialsErrorText"]
      error_text = accessor.store_element_text('login.error', conditions.displayed?)
      raise ElementNotFoundError, "The error text: #{error_text} is not: #{error_content}" unless error_text.eql?(error_content)
    end

    def become_a_member_link_exists?
      synchroniser.wait_until_element_located("login.become-a-member-link", conditions.displayed?)
    end

    def is_open?
      synchroniser.wait_until_element_located("login.sign-in-link", conditions.displayed?, :timeout => 1)
    end

    def wait_until_opened?
      synchroniser.wait_until_element_located("login.sign-in-link", conditions.displayed?)
    end

    private

    def login_email
      accessor.search_for_element("login.email", conditions.displayed?.enabled?)
    end

    def email_placeholder
      login_email.attribute("placeholder")
    end

    def email_text
      accessor.store_element_text("login.email", conditions.displayed?)
    end

    def login_password
      accessor.search_for_element("login.password", conditions.displayed?.enabled?)
    end

    def password_placeholder
      login_password.attribute("placeholder")
    end

    def password_text
      accessor.store_element_text("login.password", conditions.displayed?)
    end
  end
end
