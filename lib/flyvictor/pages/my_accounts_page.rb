module FlyVictor
  class MyAccountsPage < Page
    attr_accessor :updated_user_details_web_app

    RESTRICTED_URLS = [
        "mv/settings/mydetails",
        "mv/settings/myaddress",
        "mv/settings/notifications",
        "mv/settings/changepassword",
        "mv/settings/passengerinfo",
        "mv/settings/delegates",
        "mv/flights/upcoming",
        "mv/flights/history",
        "mv/bestoffers/made",
        "mv/bestoffers/failed",
        "mv/bestoffers/history",
        "mv/charter/quotes",
        "mv/charter/history",
        "mv/messages/inbox",
        "mv/messages/sent",
        "mv/messages/archive",
        "mv/alerts/travelprofile",
        "mv/paymenthistory"
    ]

    def initialize(runtime, locator_registry, driver)
      super(runtime, locator_registry, driver)
      @environment = runtime.environment
      @logger = runtime.logger
    end

    def check_denied_access
      failures = []
      RESTRICTED_URLS.each { |restricted_url|
        response = HTTParty.get(@environment.property("flyvictor-url") + restricted_url)
        failures << "Access to URL: #{restricted_url} was not denied." if response.request.last_uri.to_s.eql?(@environment.property("flyvictor-url") + restricted_url)
        #TODO Do we need to check that the user is redirected to login when attempting access to restricted content?
        #failures << "Attempted access to URL: #{restricted_url} failed to redirect to the login." unless response.request.last_uri.to_s.include?("#login")
      }
      failures
    end

    def click_payment_history_link
      payment_history_link = accessor.search_for_element("my-account.payment-history-link", conditions.displayed?.enabled?)
      payment_history_link.click
    end

    def is_open?
      synchroniser.wait_until_element_located("my-account.side-panel", conditions.displayed?, :timeout => 1)
    end

    def wait_until_opened?
      synchroniser.wait_until_element_located("my-account.side-panel", conditions.displayed?)
    end

    def click_settings_my_details
      settings_link = accessor.search_for_element("my-account.settings-link", conditions.displayed?.enabled?)
      settings_link.click
    end

    def random_string(number_of_letters = 7)
      Array.new(number_of_letters) { (rand(122-97) + 97).chr }.join
    end

    def random_number(number_of_digits = 11)
      rand(10 ** number_of_digits)
    end

    def add_to_change_user_details_web_app_hash(key, value)
      self.updated_user_details_web_app[key] = value
    end

    def change_basic_info
      if action.first_selected_option(title).text.eql?("Dr")
        action.select_by_text(title, "Mr")
      elsif action.first_selected_option(title).text.eql?("Mr")
        action.select_by_text(title, "Dr")
      end

      first_name.clear
      first_name.send_keys random_string

      last_name.clear
      last_name.send_keys random_string

      # Uncomment this later, otherwise you will not be able to login
      #   action.click(edit_email)

      #  synchroniser.wait_until_element_located("my-details-email-address", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)

      # Do we need to edit the email?
      #  email_textfield.clear
      #  email_textfield.send_keys "flyvictortesting+joao".concat(SecureRandom.hex(2)).concat("@gmail.com")
    end

    def change_contact_info
      company.clear
      company.send_keys random_string

      primary_phone.clear
      primary_phone.send_keys random_number

      alternative_phone.clear
      alternative_phone.send_keys random_number

      if action.first_selected_option(mobile_device).text.eql?("Apple IPhone")
        action.select_by_text(mobile_device, "Android Device")
      elsif action.first_selected_option(mobile_device).text.eql?("Android Device")
        action.select_by_text(mobile_device, "Apple iPhone")
      end
    end

    def aditional_info
      # This action will trigger a language preferences pop-up on the next login
      # if action.first_selected_option(language).text.include?("English")
      #   action.select_by_value(language, "de-DE")
      # elsif action.first_selected_option(language).text.include?("Deutsch")
      #   action.select_by_value(language, "en-GB")
      # end

      if action.first_selected_option(how_many_times).text.eql?("0")
        action.select_by_text(how_many_times, "1-10")
      elsif action.first_selected_option(how_many_times).text.eql?("1-10")
        action.select_by_text(how_many_times, "0")
      end

      if travel_with_pets_yes.selected?
        travel_with_pets_no.click
      else
        travel_with_pets_yes.click
        pet_details.clear
        pet_details.send_keys random_string
      end
      update_user_details.click
    end


    def save_my_updated_details
      add_to_change_user_details_web_app_hash("title", action.first_selected_option(title).text)
      add_to_change_user_details_web_app_hash("first_name", first_name.attribute('value'))
      add_to_change_user_details_web_app_hash("last_name", last_name.attribute('value'))
      add_to_change_user_details_web_app_hash("current_email", current_email.text)

      add_to_change_user_details_web_app_hash("company", company.attribute('value'))
      add_to_change_user_details_web_app_hash("primary_phone", primary_phone.attribute('value'))
      add_to_change_user_details_web_app_hash("alternative_phone", alternative_phone.attribute('value'))
      add_to_change_user_details_web_app_hash("mobile_device", action.first_selected_option(mobile_device).text)
      add_to_change_user_details_web_app_hash("language", action.first_selected_option(language).attribute('value'))

      if travel_with_pets_yes.selected?
        add_to_change_user_details_web_app_hash("pet_details", pet_details.attribute('value'))
      end

      add_to_change_user_details_web_app_hash("travel_with_pets_yes", travel_with_pets_yes.selected?)
      add_to_change_user_details_web_app_hash("travel_with_pets_no", travel_with_pets_no.selected?)
    end

    def save_my_updated_address
      add_to_change_user_details_web_app_hash("home_address_line_1", home_address_line_1.attribute('value'))
      add_to_change_user_details_web_app_hash("home_address_line_2", home_address_line_2.attribute('value'))
      add_to_change_user_details_web_app_hash("home_city", home_city.attribute('value'))
      add_to_change_user_details_web_app_hash("home_region", home_region.attribute('value'))
      add_to_change_user_details_web_app_hash("home_postcode", home_postcode.attribute('value'))
      add_to_change_user_details_web_app_hash("home_country", action.first_selected_option(home_country).attribute('value'))

      add_to_change_user_details_web_app_hash("billing_address_line_1", billing_address_line_1.attribute('value'))
      add_to_change_user_details_web_app_hash("billing_address_line_2", billing_address_line_2.attribute('value'))
      add_to_change_user_details_web_app_hash("billing_city", billing_city.attribute('value'))
      add_to_change_user_details_web_app_hash("billing_region", billing_region.attribute('value'))
      add_to_change_user_details_web_app_hash("billing_postcode", billing_postcode.attribute('value'))
      add_to_change_user_details_web_app_hash("billing_country", action.first_selected_option(billing_country).attribute('value'))

      add_to_change_user_details_web_app_hash("second_home_address_line_1", second_home_address_line_1.attribute('value'))
      add_to_change_user_details_web_app_hash("second_home_address_line_2", second_home_address_line_2.attribute('value'))
      add_to_change_user_details_web_app_hash("second_home_city", second_home_city.attribute('value'))
      add_to_change_user_details_web_app_hash("second_home_region", second_home_region.attribute('value'))
      add_to_change_user_details_web_app_hash("second_home_postcode", second_home_postcode.attribute('value'))
      add_to_change_user_details_web_app_hash("second_home_country", action.first_selected_option(second_home_country).attribute('value'))
    end

    def change_my_home_address
      home_address_line_1.clear
      home_address_line_1.send_keys random_string

      home_address_line_2.clear
      home_address_line_2.send_keys random_string

      home_city.clear
      home_city.send_keys random_string

      home_region.clear
      home_region.send_keys random_string

      home_postcode.clear
      home_postcode.send_keys random_string

      if action.first_selected_option(home_country).text.eql?("United Kingdom")
        action.select_by_text(home_country, "United States")
      elsif action.first_selected_option(home_country).text.eql?("United States")
        action.select_by_text(home_country, "United Kingdom")
      end
      save_home_address.click
    end

    def change_my_home_billing
      billing_address_line_1.clear
      billing_address_line_1.send_keys random_string

      billing_address_line_2.clear
      billing_address_line_2.send_keys random_string

      billing_city.clear
      billing_city.send_keys random_string

      billing_region.clear
      billing_region.send_keys random_string

      billing_postcode.clear
      billing_postcode.send_keys random_string

      if action.first_selected_option(billing_country).text.eql?("United Kingdom")
        action.select_by_text(billing_country, "United States")
      elsif action.first_selected_option(billing_country).text.eql?("United States")
        action.select_by_text(billing_country, "United Kingdom")
      end
      save_billing_address.click
    end

    def change_my_home_second_home
      second_home_address_line_1.clear
      second_home_address_line_1.send_keys random_string

      second_home_address_line_2.clear
      second_home_address_line_2.send_keys random_string

      second_home_city.clear
      second_home_city.send_keys random_string

      second_home_region.clear
      second_home_region.send_keys random_string

      second_home_postcode.clear
      second_home_postcode.send_keys random_string

      if action.first_selected_option(second_home_country).text.eql?("United Kingdom")
        action.select_by_text(second_home_country, "United States")
      elsif action.first_selected_option(second_home_country).text.eql?("United States")
        action.select_by_text(second_home_country, "United Kingdom")
      end
      save_second_home_address.click
    end

    def change_addresses
      accessor.search_for_descendant("my-account.settings-sidebar-menu", conditions.displayed?.text_contains?("My Address", true)).click
      change_my_home_address
      change_my_home_billing
      change_my_home_second_home
    end

    def change_personal_details
      self.updated_user_details_web_app = Hash.new
      change_basic_info
      change_contact_info
      aditional_info
      save_my_updated_details
      change_addresses
      save_my_updated_address
    end

    def home_address_line_1
      synchroniser.wait_until_element_located("my-details-home-address1", conditions.displayed?, :tag_description => TagDescription::INPUT)
      accessor.search_for_element("my-details-home-address1", conditions.displayed?, :tag_description => TagDescription::INPUT)
    end

    def home_address_line_2
      accessor.search_for_element("my-details-home-address2", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def home_city
      accessor.search_for_element("my-details-home-city", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def home_region
      accessor.search_for_element("my-details-home-region", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def home_postcode
      accessor.search_for_element("my-details-home-postcode", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def home_country
      accessor.search_for_element("my-details-home-country", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def save_home_address
      accessor.search_for_element("my-details-home-save-button", conditions.displayed?.enabled?, :tag_description => TagDescription::HYPERLINK)
    end

    def billing_address_line_1
      accessor.search_for_element("my-details-billing-address1", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def billing_address_line_2
      accessor.search_for_element("my-details-billing-address2", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def billing_city
      accessor.search_for_element("my-details-billing-city", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def billing_region
      accessor.search_for_element("my-details-billing-region", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def billing_postcode
      accessor.search_for_element("my-details-billing-postcode", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def billing_country
      accessor.search_for_element("my-details-billing-country", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def save_billing_address
      accessor.search_for_element("my-details-billing-save-button", conditions.displayed?.enabled?, :tag_description => TagDescription::HYPERLINK)
    end

    def second_home_address_line_1
      accessor.search_for_element("my-details-second-home-address1", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def second_home_address_line_2
      accessor.search_for_element("my-details-second-home-address2", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def second_home_city
      accessor.search_for_element("my-details-second-home-city", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def second_home_region
      accessor.search_for_element("my-details-second-home-region", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def second_home_postcode
      accessor.search_for_element("my-details-second-home-postcode", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def second_home_country
      accessor.search_for_element("my-details-second-home-country", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def save_second_home_address
      accessor.search_for_element("my-details-second-home-save-button", conditions.displayed?.enabled?, :tag_description => TagDescription::HYPERLINK)
    end

    def company
      accessor.search_for_element("my-details-company", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def primary_phone
      accessor.search_for_element("my-details-primary-phone", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def alternative_phone
      accessor.search_for_element("my-details-alternative-phone", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def mobile_device
      accessor.search_for_element("my-details-mobile-device", conditions.displayed?.enabled?, :tag_description => TagDescription::SELECT)
    end

    def language
      accessor.search_for_element("my-details-language", conditions.displayed?.enabled?, :tag_description => TagDescription::SELECT)
    end

    def how_many_times
      accessor.search_for_element("my-details-how-many-times", conditions.displayed?.enabled?, :tag_description => TagDescription::SELECT)
    end

    def travel_with_pets_yes
      accessor.search_for_element("my-details-travel-with-pet-yes", conditions.displayed?, :tag_description => TagDescription::INPUT)
    end

    def travel_with_pets_no
      accessor.search_for_element("my-details-travel-with-pet-no", conditions.displayed?, :tag_description => TagDescription::INPUT)
    end

    def pet_details
      accessor.search_for_element("my-details-pet-details", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def current_email
      accessor.search_for_element("my-details-current-email-address", conditions.displayed?, :tag_description => TagDescription::INPUT)
    end

    def email_textfield
      accessor.search_for_element("my-details-email-address", conditions.displayed?.enabled?, :tag_description => TagDescription::INPUT)
    end

    def edit_email
      accessor.search_for_element("my-details-edit-email", conditions.displayed?.enabled?, :tag_description => TagDescription::HYPERLINK)
    end

    def title
      accessor.search_for_element("my-details-title", conditions.displayed?.enabled?)
    end

    def first_name
      accessor.search_for_element("my-details-first-name", conditions.displayed?.enabled?)
    end

    def last_name
      accessor.search_for_element("my-details-last-name", conditions.displayed?.enabled?)
    end

    def update_user_details
      accessor.search_for_element("my-details-save-user-details-button", conditions.displayed?.enabled?)
    end
  end
end