module FlyVictor
  class HomePage < Page
    def initialize(runtime, locator_registry, driver, data_manager)
      super(runtime, locator_registry, driver)
      @environment = runtime.environment
      @logger = runtime.logger
      @data_manager = data_manager
    end

    def navigate_to
      action.navigate_to(@environment.property("flyvictor-url"))
    end

    # def click_search_button
    #   search_button = accessor.search_for_element("home.search-button", conditions.displayed?.enabled?)
    #   search_button.click()
    # end
    #
    # def click_partner_find_out_more_link(partner)
    #   partner_showcase = accessor.search_for_element("home.showcase-partners", conditions.displayed?.text_contains?(partner))
    #   find_out_more_link = accessor(partner_showcase).search_for_element("home.partner-find-out-more-link", conditions.displayed?.enabled?)
    #   find_out_more_link.click
    #   @current_partner = partner
    # end
    #
    # def click_news_article_read_more_link(article)
    #   news_articles = @data_manager.content["homeNewsArticleLinks"]
    #   read_more_link = accessor.search_for_descendant("home.showcase-news-articles", conditions.displayed?.enabled?.attribute_contains?("href", news_articles[article]), :tag_description => TagDescription::HYPERLINK)
    #   # Do not click on the link if using phantomjs.
    #   read_more_link.click unless @driver.capabilities[:browser_name] == 'phantomjs'
    #   @current_article_href = read_more_link.attribute("href")
    #   @current_article = article
    # end
    #
    # def click_aviation_authority_link(aviation_authority)
    #   aviation_authorities = @data_manager.content["homeAviationAuthorityFooterLinks"]
    #   aviation_authority_link = accessor.search_for_descendant("home.footer-left", conditions.displayed?.enabled?.attribute_contains?("href", aviation_authorities[aviation_authority]), :tag_description => TagDescription::HYPERLINK)
    #   aviation_authority_link.click
    #   @current_aviation_authority = aviation_authority
    # end
    #
    # def click_terms_and_conditions_link
    #   terms_and_conditions_link = accessor.search_for_element("home.terms-and-conditions-link", conditions.displayed?.enabled?)
    #   # Get the href before clicking the link otherwise the element will become stale after the DOM is refreshed.
    #   @terms_and_conditions_href = terms_and_conditions_link.attribute("href")
    #   terms_and_conditions_link.click
    # end
    #
    # def click_privacy_policy_link
    #   privacy_policy_link = accessor.search_for_element("home.privacy-policy-link", conditions.displayed?.enabled?)
    #   # Get the href before clicking the link otherwise the element will become stale after the DOM is refreshed.
    #   @privacy_policy_href = privacy_policy_link.attribute("href")
    #   privacy_policy_link.click
    # end
    #
    # def check_search_banner_text
    #   search_banner_content = @data_manager.content["homeCharterRequestHeadline"]
    #   search_banner_text = StringUtils.squish(accessor.store_element_text("home.search-banner", conditions.displayed?))
    #   raise ElementNotFoundError, "The search banner text: #{search_banner_text} is not: #{search_banner_content}" if !search_banner_text.eql?(search_banner_content)
    # end
    #
    # def check_value_propositions_text
    #   failures = []
    #   compareflights_content = @data_manager.content["homeShowcaseCompareFlights"]
    #   bestprice_content = @data_manager.content["homeShowcaseBestPrice"]
    #   unbelievableservice_content = @data_manager.content["homeShowcaseUnbelievableService"]
    #   compareflights_text = StringUtils.squish(accessor.store_element_text("home.showcase-compareflights", conditions.displayed?))
    #   bestprice_text = StringUtils.squish(accessor.store_element_text("home.showcase-bestprice", conditions.displayed?))
    #   unbelievableservice_text = StringUtils.squish(accessor.store_element_text("home.showcase-unbelievableservice", conditions.displayed?))
    #   failures << "The compare flights value proposition text: #{compareflights_text} is not: #{compareflights_content}" if !compareflights_text.eql?(compareflights_content)
    #   failures << "The best price value proposition text: #{bestprice_text} is not: #{bestprice_content}" if !bestprice_text.eql?(bestprice_content)
    #   failures << "The unbelievable service value proposition text: #{unbelievableservice_text} is not: #{unbelievableservice_content}" if !unbelievableservice_text.eql?(unbelievableservice_content)
    #   failures
    # end
    #
    # def check_operators_banner_text
    #   operators_banner_content = @data_manager.content["homeOperatorsHeadline"]
    #   operators_banner_text = StringUtils.squish(accessor.store_element_text("home.headline-banner", conditions.displayed?.text_contains?("110 Operators")))
    #   raise ElementNotFoundError, "The operators banner text: #{operators_banner_text} is not: #{operators_banner_content}" if !operators_banner_text.eql?(operators_banner_content)
    # end
    #
    # def check_aircraft_types
    #   aircraft_types_content = @data_manager.content["homeShowcaseAircraftTypes"]
    #   aircraft_types_text = accessor.store_elements_text("home.showcase-aircraft-types", conditions)
    #   aircraft_types_content - aircraft_types_text
    # end
    #
    # def check_member_benefits_banner_text
    #   member_benefits_banner_content = @data_manager.content["homeMemberBenefitsHeadline"]
    #   member_benefits_banner_text = StringUtils.squish(accessor.store_element_text("home.headline-banner", conditions.displayed?.text_contains?("Member Benefits")))
    #   raise ElementNotFoundError, "The member benefits banner text: #{member_benefits_banner_text} is not: #{member_benefits_banner_content}" if !member_benefits_banner_text.eql?(member_benefits_banner_content)
    # end
    #
    # def check_partners_text
    #   failures = []
    #   partner_overviews = @data_manager.content["homePartnerOverviews"]
    #   partner_overviews.each { |key, value|
    #     partner_text = StringUtils.squish(accessor.store_element_text("home.showcase-partners", conditions.displayed?.text_contains?(key)))
    #     failures << "The #{key} text: #{partner_text} is not: #{value}" if !partner_text.include?(value)
    #   }
    #   failures
    # end
    #
    # def check_news_banner_text
    #   news_banner_content = @data_manager.content["homeNewsHeadline"]
    #   news_banner_text = accessor.store_element_text("home.news-banner", conditions.displayed?)
    #   raise ElementNotFoundError, "The news banner text: #{news_banner_text} is not: #{news_banner_content}" if !news_banner_text.eql?(news_banner_content)
    # end
    #
    # def check_news_article_text
    #   failures = []
    #   news_articles = @data_manager.content["homeNewsArticles"]
    #   news_articles.each { |news_article|
    #     failures << "The news article #{news_article} was not displayed." if accessor.search_for_element("home.showcase-news-articles", conditions.displayed?.text_contains?(news_article), :suppress_timeout_error => true).nil?
    #   }
    #   failures
    # end
    #
    # def check_footer_links
    #   check_why_victor_links + check_services_links + check_about_us_links + check_support_links
    # end
    #
    # def check_contact_info_text
    #   contact_info_content = @data_manager.content["homeContactInfoText"]
    #   contact_info_text = StringUtils.squish(accessor.store_element_text("home.footer-middle", conditions.displayed?))
    #   raise ElementNotFoundError, "The contact information text: #{contact_info_text} is not: #{contact_info_content}" if !contact_info_text.eql?(contact_info_content)
    # end
    #
    # def value_propositions_find_out_more_link_exists?
    #   synchroniser.wait_until_element_located("home.showcase-find-out-more-link", conditions.displayed?.enabled?.text_contains?("find out more", true))
    # end
    #
    # def operators_find_out_more_link_exists?
    #   synchroniser.wait_until_element_located("home.operators-find-out-more-link", conditions.displayed?.enabled?.text_contains?("find out more", true))
    # end
    #
    # def is_partner_information_displayed?
    #   partners = @data_manager.content["homePartnerLinks"]
    #   action.current_url.end_with?(partners[@current_partner])
    # end
    #
    # def is_news_article_displayed?
    #   # If using phantomjs, we are only interested that an entity corresponding to
    #   # the requested resource is sent in the response.
    #   if @driver.capabilities[:browser_name] == 'phantomjs'
    #     response = HTTParty.get(@current_article_href)
    #     return response.code == 200
    #   end
    #
    #   news_articles = @data_manager.content["homeNewsArticleLinks"]
    #   # We are expecting a new browser window to be opened.
    #   action.switch_to_window
    #   article_url = action.current_url
    #   action.close_window
    #   article_url.include? news_articles[@current_article]
    # end
    #
    # def is_aviation_authority_information_displayed?
    #   aviation_authorities = @data_manager.content["homeAviationAuthorityFooterLinks"]
    #   # We are expecting a new browser window to be opened.
    #   action.switch_to_window
    #   aviation_authority_url = action.current_url
    #   action.close_window
    #   aviation_authority_url.include? aviation_authorities[@current_aviation_authority]
    # end
    #
    # #TODO Check something more unique on the page.
    # def is_terms_and_conditions_displayed?
    #   action.current_url.end_with?(@terms_and_conditions_href)
    # end
    #
    # #TODO Check something more unique on the page.
    # def is_privacy_policy_displayed?
    #   action.current_url.end_with?(@privacy_policy_href)
    # end

    def is_open?
      synchroniser.wait_until_element_located("quote.from", conditions.displayed?, :timeout => 1)
    end

    def wait_until_opened?
      synchroniser.wait_until_element_located("quote.from", conditions.displayed?)
    end

    # private
    #
    # def check_why_victor_links
    #   why_victor_text = @data_manager.content["homeWhyVictorText"]
    #   @data_manager.content["homeWhyVictorFooterLinks"] - footer_links_text(why_victor_text)
    # end
    #
    # def check_services_links
    #   services_text = @data_manager.content["homeServicesText"]
    #   @data_manager.content["homeServicesFooterLinks"] - footer_links_text(services_text)
    # end
    #
    # def check_about_us_links
    #   about_us_text = @data_manager.content["homeAboutUsText"]
    #   @data_manager.content["homeAboutUsFooterLinks"] - footer_links_text(about_us_text)
    # end
    #
    # def check_support_links
    #   support_text = @data_manager.content["homeSupportText"]
    #   @data_manager.content["homeSupportFooterLinks"] - footer_links_text(support_text)
    # end
    #
    # def footer_links_text(text)
    #   footer_menu = accessor.search_for_element("home.footer-menus", conditions.displayed?.text_contains?(text))
    #   accessor(footer_menu).store_elements_text("home.footer-menu-item", conditions.displayed?)
    # end
  end
end
