# encoding: UTF-8
module FlyVictor
  class UpcomingFlightsPage < Page
    def initialize(runtime, locator_registry, driver, data_manager)
      super(runtime, locator_registry, driver)
      @data_manager = data_manager
      @environment = runtime.environment
      @logger = runtime.logger
    end

    # def flight_exists?(flight_details)
    #   #FIXME Re-instate aircraft type comparison after understanding how it is identified.
    #   #synchroniser.wait_until_element_located("upcoming-flights.table-rows", conditions.text_contains?(flight_details.from).text_contains?(flight_details.to).text_contains?(flight_details.aircraft_type))
    #   (0..pagination_count).each {
    #     return true if synchroniser.wait_until_element_located("upcoming-flights.table-rows", conditions.text_contains?(flight_details.from).text_contains?(flight_details.to))
    #     click_next_link
    #   }
    #   false
    # end
    #
    # def flight_action_exists?(action)
    #   (0..pagination_count).each {
    #     return true if synchroniser.wait_until_element_located("upcoming-flights.table-rows", conditions.displayed?.text_contains?(action, true))
    #     click_next_link
    #   }
    #   false
    # end
    #
    # def selected_flight_action_exists?(action)
    #   (0..pagination_count).each {
    #     return true if synchroniser.wait_until_element_located("upcoming-flights.table-rows", conditions.displayed?.attribute_contains?("rel", "#{@upcoming_flight_details["Flight Number"]}").text_contains?(action, true))
    #     click_next_link
    #   }
    #   false
    # end
    #
    # def click_action_button(action)
    #   #Find the first upcoming flights row with the action displayed
    #   #At present further flight details are revealed when either clicking check in or view details
    #   upcoming_flights_row = accessor.search_for_element("upcoming-flights.table-rows", conditions.displayed?.text_contains?(action, true))
    #   flight_number = upcoming_flights_row.attribute("rel")
    #   flight_date_time = DateTime.parse(accessor(upcoming_flights_row).store_element_text("upcoming-flights.flight-details", conditions.displayed?.attribute_contains?("class", "FlightDate")))
    #   flight_route = accessor(upcoming_flights_row).store_element_text("upcoming-flights.flight-details", conditions.displayed?.attribute_contains?("class", "Route"))
    #   flight_aircraft_type = accessor(upcoming_flights_row).store_element_text("upcoming-flights.aircraft-type", conditions.displayed?.attribute_contains?("class", "resultactype"))
    #   @upcoming_flight_details = Hash.new
    #   @upcoming_flight_details["Flight Number"] = flight_number
    #   #Manipulate upcoming flight details to match format of flight details on passenger check in page
    #   @upcoming_flight_details["Date"] = flight_date_time.strftime("%d/%m/%Y")
    #   @upcoming_flight_details["Route Departure Time"] = flight_route.gsub(/ to /, " #{flight_date_time.strftime("%H:%M")} > ")
    #   @upcoming_flight_details["Aircraft Type"] = flight_aircraft_type
    #   action_button = accessor(upcoming_flights_row).search_for_element("upcoming-flights.check-in.actions", conditions.displayed?.text_contains?(action, true), :tag_description => TagDescription::SPAN)
    #   action_button.click
    # end
    #
    # def click_selected_action_button(action)
    #   upcoming_flights_row = accessor.search_for_element("upcoming-flights.table-rows", conditions.displayed?.attribute_contains?("rel", @upcoming_flight_details["Flight Number"]).text_contains?(action, true))
    #   action_button = accessor(upcoming_flights_row).search_for_element("upcoming-flights.check-in.actions", conditions.displayed?.text_contains?(action, true), :tag_description => TagDescription::SPAN)
    #   action_button.click
    # end
    #
    # def additional_flight_details
    #   additional_flight_details = accessor.search_for_element("upcoming-flights.additional-flight-details", conditions.displayed?)
    #   flight_aircraft = accessor(additional_flight_details).store_element_text("upcoming-flights.additional-flight-details.flight-aircraft", conditions.displayed?)
    #   flight_operator = accessor(additional_flight_details).store_element_text("upcoming-flights.additional-flight-details.flight-operator", conditions.displayed?)
    #   flight_number = accessor(additional_flight_details).store_element_text("upcoming-flights.additional-flight-details.flight-number", conditions.displayed?).squish.gsub(/Operator: #{flight_operator} Flight number: /, "")
    #   @upcoming_flight_details["Aircraft"] = flight_number == @upcoming_flight_details["Flight Number"] ? flight_aircraft : ""
    #   @upcoming_flight_details["Operator"] = flight_number == @upcoming_flight_details["Flight Number"] ? flight_operator : ""
    # end
    #
    # def additional_flight_details_displayed?
    #   #Expect additional details for one flight to be displayed
    #   synchroniser.wait_until_element_located("upcoming-flights.additional-flight-details", conditions.displayed?)
    # end
    #
    # def click_first_seating_allocation_link
    #   first_seating_allocation_link = accessor.search_for_element("upcoming-flights.additional-flight-details.seating-allocation-links", conditions.displayed?.enabled?, :tag_description => TagDescription::HYPERLINK)
    #   first_seating_allocation_link.click
    # end
    #
    # def click_next_link
    #   next_link = accessor.search_for_element("upcoming-flights.next-link", conditions.displayed?, :suppress_timeout_error => true)
    #   next_link.click unless next_link.nil?
    # end
    #
    # def check_passenger_check_in_instructions_exist?
    #   instructions_content = @data_manager.content["passengerCheckIn"]
    #   page_instructions = accessor.store_element_text("passenger-check-in-page.instructional-text", conditions.displayed?)
    #   instructions_content.downcase.eql? page_instructions.squish.downcase
    # end
    #
    # def wait_until_passenger_check_in_opened?
    #   #TODO: Reorganise in passenger check in page
    #   synchroniser.wait_until_element_located("passenger-check-in-page.header", conditions.displayed?, :timeout => 30)
    # end

    def wait_until_opened?
      synchroniser.wait_until_element_located("upcoming-flights.badge", conditions.displayed?, :timeout => 30)
    end

    # def check_flight_details
    #   failures = []
    #   upcoming_flight_keys = @upcoming_flight_details.keys
    #   upcoming_flight_keys.each { |key|
    #     failures << "#{key} text was not #{@upcoming_flight_details[key]} on the check in page." unless passenger_check_in_flight_details_text.include? @upcoming_flight_details[key]
    #   }
    #   failures
    # end
    #
    # def add_new_passenger?
    #   synchroniser.wait_until_element_located("passenger-check-in-page.add-new-passenger", conditions.displayed?.enabled?)
    # end
    #
    # def click_add_new_passenger_button
    #   add_new_passenger_button = accessor.search_for_element("passenger-check-in-page.add-new-passenger", conditions.displayed?.enabled?)
    #   add_new_passenger_button.click
    # end
    #
    # def do_enter_new_passenger_name(decision)
    #   #TODO If multiple passengers consider an array of hashes
    #   @new_passenger_details = Hash.new
    #   case decision
    #     when "entering"
    #       @new_passenger_details["Title"] = "Mrs"
    #       @new_passenger_details["First Name"] = random_string([*5..15].sample).capitalize
    #       @new_passenger_details["Last Name"] = random_string([*5..15].sample).capitalize
    #       select_new_passenger_title(@new_passenger_details["Title"])
    #       enter_new_passenger_first_name(@new_passenger_details["First Name"])
    #       enter_new_passenger_last_name(@new_passenger_details["Last Name"])
    #   end
    #   click_add_new_passenger_button
    # end
    #
    # def do_enter_new_passenger_details(action, mandatory = false)
    #   departure_date = Date.parse(@upcoming_flight_details["Date"])
    #   issue_date = (departure_date - 365.days).strftime("%d/%m/%Y")
    #   expiry_date = (departure_date + 365.days).strftime("%d/%m/%Y")
    #
    #   #TODO Set up to use data json
    #   title = "Prof"
    #   first_name = "Brian"
    #   last_name = "Cox"
    #   passport_number = "1232456789"
    #   date_of_birth = "01/01/1970"
    #   nationality = "British"
    #
    #   select_passenger_title(title) unless action.eql?("omit")
    #   enter_passenger_first_name(first_name) unless action.eql?("omit")
    #   enter_passenger_last_name(last_name) unless mandatory && action.eql?("enter")
    #   enter_passenger_passport_number(passport_number) unless action.eql?("omit")
    #   enter_passenger_passport_issue_date(issue_date) unless action.eql?("omit")
    #   enter_passenger_passport_expiry_date(expiry_date) unless action.eql?("omit")
    #   enter_passenger_date_of_birth(date_of_birth) unless action.eql?("omit")
    #   select_passenger_nationality(nationality) unless action.eql?("omit")
    #   #FIXME Remove work around used to code subsequent steps
    #   #The title entered is not picked up when the new user is created
    #   #@new_passenger_name = mandatory ? "#{title} #{first_name}" : "#{title} #{first_name} #{last_name}"
    #   @new_passenger_name = mandatory ? "Mr #{first_name}" : "Mr #{first_name} #{last_name}"
    # end
    #
    # def do_invalidate_new_passenger_details(field)
    #   departure_date = Date.parse(@upcoming_flight_details["Date"])
    #   issue_date = (departure_date - 365.days).strftime("%d/%m/%Y")
    #   expiry_date = (departure_date + 365.days).strftime("%d/%m/%Y")
    #
    #   #TODO Set up to use data json
    #   title = "Prof"
    #   first_name = "Brian1"
    #   last_name = "Cox"
    #   passport_number = "1232456789"
    #   date_of_birth = "01/01/1970"
    #   nationality = "British"
    #
    #   #Invalid data examples
    #   invalid_first_name = "Jo$eph"
    #   invalid_last_name = "Blogg$"
    #   invalid_passport_number = "1234$6789"
    #   invalid_issue_date = (departure_date - 365.days).strftime("%Y-%m")
    #   invalid_expiry_date = (departure_date + 365.days).strftime("%Y-%m")
    #   invalid_date_of_birth = Date.parse(date_of_birth).strftime("%Y-%m")
    #
    #   select_passenger_title(title)
    #   field.include?("first name") ? enter_passenger_first_name(invalid_first_name) : enter_passenger_first_name(first_name)
    #   field.include?("last name") ? enter_passenger_last_name(invalid_last_name) : enter_passenger_last_name(last_name)
    #   field.include?("passport number") ? enter_passenger_passport_number(invalid_passport_number) : enter_passenger_passport_number(passport_number)
    #   field.include?("issue date") ? enter_passenger_passport_issue_date(invalid_issue_date) : enter_passenger_passport_issue_date(issue_date)
    #   field.include?("expiry date") ? enter_passenger_passport_expiry_date(invalid_expiry_date) : enter_passenger_passport_expiry_date(expiry_date)
    #   field.include?("date of birth") ? enter_passenger_date_of_birth(invalid_date_of_birth) : enter_passenger_date_of_birth(date_of_birth)
    #   select_passenger_nationality(nationality)
    #   #The title entered is not picked up when the new user is created
    #   #@new_passenger_name = "#{title} #{field.include?("first name") ? invalid_first_name : first_name} #{field.include?("last name") ? invalid_last_name : last_name}"
    #   #FIXME Should pick up invalid name
    #   @new_passenger_name = "Mr #{field.include?("first name") ? invalid_first_name : first_name} #{field.include?("last name") ? invalid_last_name : last_name}"
    # end
    #
    #
    # def check_mandatory_notifications_hidden?
    #   synchroniser.wait_until_element_located("passenger-check-in-page.passenger-details.mandatory-name", conditions.hidden?) &&
    #       synchroniser.wait_until_element_located("passenger-check-in-page.passenger-details.mandatory-passport-number", conditions.hidden?) &&
    #       synchroniser.wait_until_element_located("passenger-check-in-page.passenger-details.mandatory-passport-issue-date", conditions.hidden?) &&
    #       synchroniser.wait_until_element_located("passenger-check-in-page.passenger-details.mandatory-passport-expiry-date", conditions.hidden?) &&
    #       synchroniser.wait_until_element_located("passenger-check-in-page.passenger-details.mandatory-date-of-birth", conditions.hidden?) &&
    #       synchroniser.wait_until_element_located("passenger-check-in-page.passenger-details.mandatory-nationality", conditions.hidden?)
    # end
    #
    # def check_mandatory_notifications
    #   name_content = @data_manager.content["newPassengerMandatoryName"]
    #   passport_number_content = @data_manager.content["newPassengerMandatoryPassportNumber"]
    #   passport_issue_date_content = @data_manager.content["newPassengerMandatoryPassportIssueDate"]
    #   passport_expiry_date_content = @data_manager.content["newPassengerMandatoryPassportExpiryDate"]
    #   date_of_birth_content = @data_manager.content["newPassengerMandatoryDateOfBirth"]
    #   nationality_content = @data_manager.content["newPassengerMandatoryNationality"]
    #   failures = []
    #   failures << "The mandatory name notification is not #{name_content}." unless passenger_details_mandatory_name_text.eql?(name_content)
    #   failures << "The mandatory passport number notification is not #{passport_number_content}." unless passenger_details_mandatory_passport_number_text.eql?(passport_number_content)
    #   failures << "The mandatory passport issue date notification is not #{passport_issue_date_content}." unless passenger_details_mandatory_passport_issue_date_text.eql?(passport_issue_date_content)
    #   failures << "The mandatory passport expiry date notification is not #{passport_expiry_date_content}." unless passenger_details_mandatory_passport_expiry_date_text.eql?(passport_expiry_date_content)
    #   failures << "The mandatory date of birth notification is not #{date_of_birth_content}." unless passenger_details_mandatory_date_of_birth_text.eql?(date_of_birth_content)
    #   failures << "The mandatory nationality notification is not #{nationality_content}." unless passenger_details_mandatory_nationality_text.eql?(nationality_content)
    #   failures
    # end
    #
    # def check_passenger_in_first_seat_allocation?
    #   #passenger_seat_allocation_select = accessor.store_element_text("passenger-check-in-page.seat-allocations", conditions, :tag_description => TagDescription::SELECT)
    #   #passenger_seat_allocation_select.include? @new_passenger_name
    #   seat_allocation_passenger_list.each { |passenger|
    #     return true if passenger.eql?(@new_passenger_name)
    #   }
    #   false
    # end
    #
    # def allocate_new_passenger_to_first_seat
    #   first_seat = accessor.search_for_element("passenger-check-in-page.seat-allocations", conditions, :tag_description => TagDescription::SELECT)
    #   action.select_by_text(first_seat, @new_passenger_name)
    # end
    #
    # def allocate_passenger_to_first_seat
    #   #TODO Revise method to allocate passengers to seats with an input for the number of passengers allocated based on the seat count
    #   first_seat = accessor.search_for_element("passenger-check-in-page.seat-allocations", conditions, :tag_description => TagDescription::SELECT)
    #   action.select_by_text(first_seat, seat_allocation_passenger_list[0])
    # end
    #
    # def allocate_passengers_to_seats(number)
    #   seat_allocation_details = []
    #   seat_dropdowns = accessor.search_for_elements("passenger-check-in-page.seat-dropdowns", conditions.displayed?)
    #   allocate = number > seat_dropdowns.size ? seat_dropdowns.size : number
    #   seat_dropdowns.each_with_index { |seat, index|
    #     #FIXME Need to account for number of passengers
    #     seat_select = accessor(seat).search_for_element("passenger-check-in-page.seat-passenger", conditions, :tag_description => TagDescription::SELECT)
    #     action.select_by_text(seat_select, seat_allocation_passenger_list[index]) if index < allocate
    #     passenger = action.first_selected_option(seat_select).text.eql?("Empty Seat") ? "" : action.first_selected_option(seat_select).text
    #     details = Hash.new
    #     details["Seat Number"] = accessor(seat).store_element_text("passenger-check-in-page.seat-number", conditions.displayed?.text_contains?("seat", true))
    #     details["Seat Passenger"] = passenger
    #     seat_allocation_details << details
    #   }
    #   @check_in_seat_allocations = seat_allocation_details
    # end
    #
    # def check_upcoming_flight_seat_allocations?
    #   seat_allocation_details = []
    #   seat_allocation_rows = accessor.search_for_elements("upcoming-flights.view-details.seat-allocations", conditions.displayed?)
    #   seat_allocation_rows.each { |seat|
    #     details = Hash.new
    #     details["Seat Number"] = accessor(seat).store_element_text("upcoming-flights.view-details.seat-number", conditions.displayed?).gsub(/:/,"")
    #     details["Seat Passenger"] = accessor(seat).store_element_text("upcoming-flights.view-details.seat-passenger", conditions.displayed?).gsub(/ \(Checked In\) /,"")
    #     seat_allocation_details << details
    #   }
    #   seat_allocation_details == @check_in_seat_allocations
    # end
    #
    # def click_passenger_check_in_button
    #   check_in_button = accessor.search_for_element("passenger-check-in-page.check-in", conditions.displayed?.enabled?)
    #   check_in_button.click
    # end
    #
    # def wait_until_new_passenger_details_cleared?
    #   new_passenger_names_list_content = @data_manager.content["newPassengerNamesListDefault"]
    #   synchroniser.wait_until_element_located("passenger-check-in-page.passenger-details.names-list", conditions.displayed?.attribute_contains?("value", new_passenger_names_list_content), :timeout => 30)
    # end
    #
    # #Select/enter "new" refers to the passenger check in page before add passenger is clicked
    # def select_new_passenger_title(title)
    #   #TODO Randomise selected title
    #   action.select_by_value(new_passenger_title_select, title)
    # end
    #
    # def enter_new_passenger_first_name(first_name)
    #   new_passenger_first_name = accessor.search_for_element("passenger-check-in-page.add-new-passenger.first-name", conditions.displayed?)
    #   new_passenger_first_name.send_keys first_name
    # end
    #
    # def enter_new_passenger_last_name(last_name)
    #   new_passenger_last_name = accessor.search_for_element("passenger-check-in-page.add-new-passenger.last-name", conditions.displayed?)
    #   new_passenger_last_name.send_keys last_name
    # end
    #
    # def select_passenger_title(title)
    #   passenger_title = accessor.search_for_element("passenger-check-in-page.passenger-details.title", conditions.displayed?)
    #   action.select_by_value(passenger_title, title)
    # end
    #
    # def enter_passenger_first_name(first_name)
    #   passenger_first_name = accessor.search_for_element("passenger-check-in-page.passenger-details.first-name", conditions.displayed?)
    #   passenger_first_name.send_keys first_name
    # end
    #
    # def enter_passenger_last_name(last_name)
    #   new_passenger_last_name = accessor.search_for_element("passenger-check-in-page.passenger-details.last-name", conditions.displayed?)
    #   new_passenger_last_name.send_keys last_name
    # end
    #
    # def enter_passenger_passport_number(passport_number)
    #   passenger_passport_number = accessor.search_for_element("passenger-check-in-page.passenger-details.passport-number", conditions.displayed?)
    #   passenger_passport_number.send_keys passport_number
    # end
    #
    # def enter_passenger_passport_issue_date(issue_date)
    #   passenger_passport_issue_date = accessor.search_for_element("passenger-check-in-page.passenger-details.passport-issue-date", conditions.displayed?)
    #   passenger_passport_issue_date.send_keys issue_date
    # end
    #
    # def enter_passenger_passport_expiry_date(expiry_date)
    #   passenger_passport_expiry_date = accessor.search_for_element("passenger-check-in-page.passenger-details.passport-expiry-date", conditions.displayed?)
    #   passenger_passport_expiry_date.send_keys expiry_date
    # end
    #
    # def enter_passenger_date_of_birth(date_of_birth)
    #   passenger_date_of_birth = accessor.search_for_element("passenger-check-in-page.passenger-details.date_of_birth", conditions.displayed?)
    #   passenger_date_of_birth.send_keys date_of_birth
    # end
    #
    # def select_passenger_nationality(nationality)
    #   passenger_nationality = accessor.search_for_element("passenger-check-in-page.passenger-details.nationality", conditions.displayed?)
    #   action.select_by_text(passenger_nationality, nationality)
    # end
    #
    # def click_save_new_passenger_button
    #   save_button = accessor.search_for_element("passenger-check-in-page.passenger-details.save", conditions.displayed?.enabled?)
    #   save_button.click
    # end
    #
    # def click_cancel_new_passenger_button
    #   cancel_button = accessor.search_for_element("passenger-check-in-page.passenger-details.cancel", conditions.displayed?.enabled?)
    #   cancel_button.click
    # end
    #
    # def new_passenger_details_closed?
    #   #Timeout changed to 120 seconds to wait for form to close
    #   synchroniser.wait_until_element_located("passenger-check-in-page.passenger-details.cancel", conditions.hidden?, :timeout => 120)
    # end
    #
    # def check_new_passenger_title_default
    #   title_default_content = @data_manager.content["newPassengerTitleDefault"]
    #   title_text = action.first_selected_option(new_passenger_title_select).attribute("value")
    #   failure = "The title default is not #{title_default_content}." unless title_text.eql?(title_default_content)
    #   failure
    # end
    #
    # def check_new_passenger_placeholders
    #   failures = []
    #   first_name_placeholder_content = @data_manager.content["newPassengerFirstNamePlaceholder"]
    #   last_name_placeholder_content = @data_manager.content["newPassengerLastNamePlaceholder"]
    #   failures << "The first name placeholder is not #{first_name_placeholder_content}." unless new_passenger_first_name_placeholder.eql?(first_name_placeholder_content)
    #   failures << "The first name text is not empty." unless new_passenger_first_name_text.eql?('')
    #   failures << "The last name placeholder is not #{last_name_placeholder_content}." unless new_passenger_last_name_placeholder.eql?(last_name_placeholder_content)
    #   failures << "The last name text is not empty." unless new_passenger_last_name_text.eql?('')
    #   failures
    # end
    #
    # def do_check_initial_passenger_details
    #   #FIXME Need to account for the error notifications being displayed when the form is loaded
    #   #There is a delay before the added user is displayed with the first passenger displayed instead
    #   #Note: This is an issue if the user has passengers added already
    #   #The mandatory field notifications are displayed when the form is loaded
    #   #TODO Add a wait for the form to clear?
    #   @add_passenger_details = Hash.new
    #   @add_passenger_details["Title"] = passenger_details_title
    #   @add_passenger_details["First Name"] = passenger_details_first_name_text
    #   @add_passenger_details["Last Name"] = passenger_details_last_name_text
    #
    #   failures = []
    #   keys = @new_passenger_details.keys
    #   keys.each { |key|
    #     failures << "Differences between [#{key}] values [#{@add_passenger_details[key]}] and [#{@new_passenger_details[key]}]." unless @add_passenger_details[key].eql? @new_passenger_details[key]
    #   }
    #   failures
    # end
    #
    # def check_passenger_details_defaults
    #   title_default_content = @data_manager.content["newPassengerTitleDefault"]
    #   nationality_default_content = @data_manager.content["newPassengerNationalityDefault"]
    #   failures = []
    #   unless @new_passenger_details.empty?
    #     failures << "The title default is not #{title_default_content}." unless passenger_details_title.eql?(title_default_content)
    #   end
    #   failures << "The nationality default is not #{nationality_default_content}." unless passenger_details_nationality.eql?(nationality_default_content)
    #   failures
    # end
    #
    # def check_passenger_details_placeholders
    #   #TODO Need to revise for pre-populated name data
    #   first_name_placeholder_content = @data_manager.content["newPassengerFirstNamePlaceholder"]
    #   last_name_placeholder_content = @data_manager.content["newPassengerLastNamePlaceholder"]
    #   passport_number_placeholder_content = @data_manager.content["newPassengerPassportNumberPlaceholder"]
    #   passport_issue_date_placeholder_content = @data_manager.content["newPassengerPassportIssueDatePlaceholder"]
    #   passport_expiry_date_placeholder_content = @data_manager.content["newPassengerPassportExpiryDatePlaceholder"]
    #   date_of_birth_placeholder_content = @data_manager.content["newPassengerDateOfBirthPlaceholder"]
    #   failures = []
    #   unless @new_passenger_details.empty?
    #     failures << "The first name placeholder is not #{first_name_placeholder_content}." unless passenger_details_first_name_placeholder.eql?(first_name_placeholder_content)
    #     failures << "The first name is not empty." unless passenger_details_first_name_text.eql?('')
    #     failures << "The last name placeholder is not #{last_name_placeholder_content}." unless passenger_details_last_name_placeholder.eql?(last_name_placeholder_content)
    #     failures << "The last name is not empty." unless passenger_details_last_name_text.eql?('')
    #   end
    #   failures << "The passport number placeholder is not #{passport_number_placeholder_content}." unless passenger_details_passport_number_placeholder.eql?(passport_number_placeholder_content)
    #   failures << "The passport number is not empty." unless passenger_details_passport_number_text.eql?('')
    #   failures << "The passport issue date placeholder is not #{passport_issue_date_placeholder_content}." unless passenger_details_passport_issue_date_placeholder.eql?(passport_issue_date_placeholder_content)
    #   failures << "The passport issue date is not empty." unless passenger_details_passport_issue_date_text.eql?('')
    #   failures << "The passport expiry date placeholder is not #{passport_expiry_date_placeholder_content}." unless passenger_details_passport_expiry_date_placeholder.eql?(passport_expiry_date_placeholder_content)
    #   failures << "The passport expiry date is not empty." unless passenger_details_passport_expiry_date_text.eql?('')
    #   failures << "The date of birth placeholder is not #{date_of_birth_placeholder_content}." unless passenger_details_date_of_birth_placeholder.eql?(date_of_birth_placeholder_content)
    #   failures << "The date of birth is not empty." unless passenger_details_date_of_birth_text.eql?('')
    #   failures
    # end
    #
    # def passenger_details_displayed?
    #   synchroniser.wait_until_element_located("passenger-check-in-page.passenger-details", conditions.displayed?)
    # end
    #
    # def passenger_information_displayed?
    #   sidebar_settings = accessor.search_for_element("victor-side-bar.panel", conditions.displayed?.attribute_contains?("href", "settings"))
    #   sidebar_settings.click
    #   passenger_information = accessor.search_for_element("victor-side-bar.sub-panel", conditions.displayed?.attribute_contains?("href", "passengerinfo"))
    #   passenger_information.click
    #   synchroniser.wait_until_element_located("passenger-information.heading", conditions.displayed?.text_contains?("passenger info", true))
    # end
    #
    # def new_passenger_information_stored?
    #   passenger_list = accessor.store_element_text("passenger-information.names-list", conditions.displayed?)
    #   passenger_list.include? @new_passenger_name
    # end

    # private
    #
    # def pagination_count
    #   upcoming_flights_count = accessor.store_element_text("upcoming-flights.badge", conditions)
    #   (upcoming_flights_count.to_i / 4).floor
    # end
    #
    # def passenger_check_in_flight_details_text
    #   accessor.store_element_text("passenger-check-in-page.flight-details", conditions)
    # end
    #
    # def new_passenger_title_select
    #   accessor.search_for_element("passenger-check-in-page.add-new-passenger.title", conditions)
    # end
    #
    # def new_passenger_first_name_placeholder
    #   accessor.search_for_element("passenger-check-in-page.add-new-passenger.first-name", conditions).attribute('placeholder')
    # end
    #
    # def new_passenger_first_name_text
    #   accessor.store_element_text("passenger-check-in-page.add-new-passenger.first-name", conditions)
    # end
    #
    # def new_passenger_last_name_placeholder
    #   accessor.search_for_element("passenger-check-in-page.add-new-passenger.last-name", conditions).attribute('placeholder')
    # end
    #
    # def new_passenger_last_name_text
    #   accessor.store_element_text("passenger-check-in-page.add-new-passenger.last-name", conditions)
    # end
    #
    # def passenger_details_title
    #   passenger_title = accessor.search_for_element("passenger-check-in-page.passenger-details.title", conditions.displayed?)
    #   action.first_selected_option(passenger_title).text
    # end
    #
    # def passenger_details_nationality
    #   passenger_nationality = accessor.search_for_element("passenger-check-in-page.passenger-details.nationality", conditions.displayed?)
    #   action.first_selected_option(passenger_nationality).text
    # end
    #
    # def passenger_details_first_name_placeholder
    #   accessor.search_for_element("passenger-check-in-page.passenger-details.first-name", conditions).attribute('placeholder')
    # end
    #
    # def passenger_details_first_name_text
    #   accessor.store_element_text("passenger-check-in-page.passenger-details.first-name", conditions.displayed?)
    # end
    #
    # def passenger_details_last_name_placeholder
    #   accessor.search_for_element("passenger-check-in-page.passenger-details.last-name", conditions).attribute('placeholder')
    # end
    #
    # def passenger_details_last_name_text
    #   accessor.store_element_text("passenger-check-in-page.passenger-details.last-name", conditions.displayed?)
    # end
    #
    # def passenger_details_passport_number_placeholder
    #   accessor.search_for_element("passenger-check-in-page.passenger-details.passport-number", conditions).attribute('placeholder')
    # end
    #
    # def passenger_details_passport_number_text
    #   accessor.store_element_text("passenger-check-in-page.passenger-details.passport-number", conditions.displayed?)
    # end
    #
    # def passenger_details_passport_issue_date_placeholder
    #   accessor.search_for_element("passenger-check-in-page.passenger-details.passport-issue-date", conditions).attribute('placeholder')
    # end
    #
    # def passenger_details_passport_issue_date_text
    #   accessor.store_element_text("passenger-check-in-page.passenger-details.passport-issue-date", conditions.displayed?)
    # end
    #
    # def passenger_details_passport_expiry_date_placeholder
    #   accessor.search_for_element("passenger-check-in-page.passenger-details.passport-expiry-date", conditions).attribute('placeholder')
    # end
    #
    # def passenger_details_passport_expiry_date_text
    #   accessor.store_element_text("passenger-check-in-page.passenger-details.passport-expiry-date", conditions.displayed?)
    # end
    #
    # def passenger_details_date_of_birth_placeholder
    #   accessor.search_for_element("passenger-check-in-page.passenger-details.date_of_birth", conditions).attribute('placeholder')
    # end
    #
    # def passenger_details_date_of_birth_text
    #   accessor.store_element_text("passenger-check-in-page.passenger-details.date_of_birth", conditions.displayed?)
    # end
    #
    # def passenger_details_mandatory_name_text
    #   accessor.store_element_text("passenger-check-in-page.passenger-details.mandatory-name", conditions.displayed?)
    # end
    #
    # def passenger_details_mandatory_passport_number_text
    #   accessor.store_element_text("passenger-check-in-page.passenger-details.mandatory-passport-number", conditions.displayed?)
    # end
    #
    # def passenger_details_mandatory_passport_issue_date_text
    #   accessor.store_element_text("passenger-check-in-page.passenger-details.mandatory-passport-issue-date", conditions.displayed?)
    # end
    #
    # def passenger_details_mandatory_passport_expiry_date_text
    #   accessor.store_element_text("passenger-check-in-page.passenger-details.mandatory-passport-expiry-date", conditions.displayed?)
    # end
    #
    # def passenger_details_mandatory_date_of_birth_text
    #   accessor.store_element_text("passenger-check-in-page.passenger-details.mandatory-date-of-birth", conditions.displayed?)
    # end
    #
    # def passenger_details_mandatory_nationality_text
    #   accessor.store_element_text("passenger-check-in-page.passenger-details.mandatory-nationality", conditions.displayed?)
    # end
    #
    # def seat_allocation_passenger_list
    #   accessor.store_elements_text("passenger-check-in-page.seat-allocation-option", conditions.text_excludes?("empty seat", true), :tag_description => TagDescription::OPTION)
    # end
    #
    # def random_string(number_of_letters = 7)
    #   Array.new(number_of_letters) { (rand(122-97) + 97).chr }.join
    # end
  end
end