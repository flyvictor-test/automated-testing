module FlyVictor
  class JoinPage < Page
    def initialize(runtime, locator_registry, driver, data_manager)
      super(runtime, locator_registry, driver)
      @environment = runtime.environment
      @logger = runtime.logger
      @data_manager = data_manager
    end

    def do_setup_password
      enter_setup_password("anon1984")
      enter_confirm_setup_password("anon1984")
      click_save_password_button
      raise ElementNotFoundError, "The save password success panel was not displayed." unless wait_until_success_panel_opened?
      click_close_icon
      raise ElementNotFoundError, "The delegate popup was not closed." unless wait_until_delegate_popup_closed?
    end

    def click_join_link
      join_link = accessor.search_for_element("join.link", conditions.displayed?.enabled?)
      join_link.click
    end

    def enter_personal_details(user, mandatory = false)
      enter_email(user.email)
      enter_password(user.password)
      confirm_password(user.password)
      enter_phone_number(user.phone_number) unless mandatory
      select_title(user.title)
      enter_firstname(user.firstname)
      enter_lastname(user.lastname)
      select_country(user.country)
      select_language(user.language)
      select_hours(user.hours_flown)
    end

    def invalidate_personal_details(field)
      enter_email("flyvictortesting+#{DateTime.now.strftime("%d%m%y%H%M%S")}@gmail.com") unless field.eql?("email address")
      enter_password("anon1984") unless field.include?("password")
      confirm_password("anon1984") unless field.include?("password")
      enter_phone_number('01225 100200') unless field.eql?("phone number")
      select_title('Mr')
      enter_firstname("Jackie")
      enter_lastname('Chan')
      select_country('GB')
      select_language('en-GB')
      select_hours('11-25')

      # Input text into the invalid field last so that form level validation is exercised.
      case field
      when "email address"
        enter_email("@flyictor.com")
      when "number of password characters"
        confirm_password("anon")
        enter_password("anon")
      when "mismatching password"
        enter_password("anon1984")
        confirm_password("anon1985")
      when "phone number"
        enter_phone_number("o2082739290")
      end
    end

    def shift_focus
      registration_form_header = accessor.search_for_element("join.registration-form-header", conditions.displayed?)
      registration_form_header.click
    end

    def enter_email(username)
      join_email.send_keys username
    end

    def enter_password(password)
      join_password.send_keys password
    end

    def confirm_password(password)
      join_confirm_password.send_keys password
    end

    def enter_phone_number(phone_number)
      join_phone.send_keys phone_number
    end

    def select_title(title)
      action.select_by_text(join_title, title)
    end

    def enter_firstname(firstname)
      join_firstname.send_keys firstname
    end

    def enter_lastname(lastname)
      join_lastname.send_keys lastname
    end

    def select_country(country)
      action.select_by_value(join_country, country)
    end

    def select_language(language)
      action.select_by_value(join_language, language)
    end

    def select_hours(hours)
      action.select_by_text(join_hours, hours)
    end

    def click_victor_terms_and_conditions_link
      terms_and_conditions_link = accessor.search_for_child("join.terms-and-conditions-group", conditions.displayed?.enabled?, :tag_description => TagDescription::HYPERLINK)
      terms_and_conditions_link.click
    end

    def is_victor_terms_and_conditions_displayed?
      action.switch_to_window
      title = action.window_title
      action.close_window
      title.eql? 'Victor Member\'s Terms & Conditions'
    end

    def click_terms_and_conditions_checkbox
      terms_and_conditions_checkbox = accessor.search_for_element("join.terms-and-conditions-checkbox", conditions.displayed?.enabled?)
      terms_and_conditions_checkbox.click
    end

    def click_create_account_button
      create_account_button = accessor.search_for_element("join.create-account-button", conditions.displayed?.enabled?)
      create_account_button.click
    end

    def enter_setup_password(password)
      setup_password = accessor.search_for_element("join.delegate-popup.password", conditions.displayed?.enabled?)
      setup_password.send_keys password
    end

    def enter_confirm_setup_password(password)
      confirm_setup_password = accessor.search_for_element("join.delegate-popup.confirm-password", conditions.displayed?.enabled?)
      confirm_setup_password.send_keys password
    end

    def click_save_password_button
      save_password_button = accessor.search_for_element("join.delegate-popup.save-password-button", conditions.displayed?.enabled?)
      save_password_button.click
    end

    def click_close_icon
      close_icon = accessor.search_for_element("join.delegate-popup.close-icon", conditions.displayed?.enabled?)
      close_icon.click
    end

    def check_titles
      @data_manager.content["titles"] - titles
    end

    def check_countries
      @data_manager.content["countries"] - countries
    end

    def check_languages
      @data_manager.content["languages"] - languages
    end

    def check_hours_flown
      @data_manager.content["hoursFlown"] - hours
    end

    def check_registration_defaults
      failures = []
      default_title_content = @data_manager.content["titleDefault"]
      default_country_content = @data_manager.content["countryDefault"]
      default_language_content = @data_manager.content["languageDefault"]
      default_hours_flown_content = @data_manager.content["hoursFlownDefault"]
      failures << "The default title selection is not #{default_title_content}." if !title_text.eql?(default_title_content)
      failures << "The default country selection is not #{default_country_content}." if !country_text.eql?(default_country_content)
      failures << "The default language selection is not #{default_language_content}." if !language_text.eql?(default_language_content)
      failures << "The default hours flown selection is not #{default_hours_flown_content}." if !hours_text.eql?(default_hours_flown_content)
      failures
    end

    def check_registration_placeholders
      failures = []
      email_address_placeholder_content = @data_manager.content["emailAddressPlaceholder"]
      password_placeholder_content = @data_manager.content["passwordPlaceholder"]
      confirm_password_placeholder_content = @data_manager.content["confirmPasswordPlaceholder"]
      phone_number_placeholder_content = @data_manager.content["phoneNumberPlaceholder"]
      firstname_placeholder_content = @data_manager.content["firstNamePlaceholder"]
      lastname_placeholder_content = @data_manager.content["lastNamePlaceholder"]
      failures << "The email address placeholder: #{email_placeholder} is not: #{email_address_placeholder_content}." if !email_placeholder.eql?(email_address_placeholder_content)
      failures << "The email address text is not empty." if !email_text.eql?('')
      failures << "The password placeholder: #{password_placeholder} is not: #{password_placeholder_content}." if !password_placeholder.eql?(password_placeholder_content)
      failures << "The password text is not empty." if !password_text.eql?('')
      failures << "The confirmation password placeholder: #{confirm_password_placeholder} is not: #{confirm_password_placeholder_content}." if !confirm_password_placeholder.eql?(confirm_password_placeholder_content)
      failures << "The confirmation password text is not empty." if !confirm_password_text.eql?('')
      failures << "The phone number placeholder: #{phone_number_placeholder} is not: #{phone_number_placeholder_content}." if !phone_number_placeholder.eql?(phone_number_placeholder_content)
      failures << "The phone number text is not empty." if !phone_number_text.eql?('')
      failures << "The first name placeholder: #{firstname_placeholder} is not: #{firstname_placeholder_content}." if !firstname_placeholder.eql?(firstname_placeholder_content)
      failures << "The first name text is not empty." if !firstname_text.eql?('')
      failures << "The last name placeholder: #{lastname_placeholder} is not: #{lastname_placeholder_content}." if !lastname_placeholder.eql?(lastname_placeholder_content)
      failures << "The last name text is not empty." if !lastname_text.eql?('')
      failures
    end

    def check_already_registered_email_error_text
      error_content = @data_manager.content["joinAlreadyRegisteredEmailErrorText"]
      error_text = accessor.store_element_text("join.already-registered-email-error", conditions.displayed?)
      raise ElementNotFoundError, "The error text: #{error_text} is not: #{error_content}" if !error_text.eql?(error_content)
    end

    def check_invalid_email_error_text
      error_content = @data_manager.content["joinInvalidEmailErrorText"]
      error_text = accessor.store_element_text("join.invalid-email-error", conditions.displayed?)
      raise ElementNotFoundError, "The error text: #{error_text} is not: #{error_content}" if !error_text.eql?(error_content)
    end

    def check_invalid_number_of_password_characters_error_text
      error_content = @data_manager.content["joinInvalidNumberOfPasswordCharactersErrorText"]
      error_text = accessor.store_element_text("join.invalid-number-of-password-characters-error", conditions.displayed?)
      raise ElementNotFoundError, "The error text: #{error_text} is not: #{error_content}" if !error_text.eql?(error_content)
    end

    def check_unmatched_passwords_error_text
      error_content = @data_manager.content["joinUnmatchedPasswordsErrorText"]
      error_text = accessor.store_element_text("join.unmatched-passwords-error", conditions.displayed?)
      raise ElementNotFoundError, "The error text: #{error_text} is not: #{error_content}" if !error_text.eql?(error_content)
    end

    def check_invalid_phone_number_characters_error_text
      error_content = @data_manager.content["joinInvalidPhoneNumberCharactersErrorText"]
      error_text = accessor.store_element_text("join.invalid-phone-number-characters-error", conditions.displayed?)
      raise ElementNotFoundError, "The error text: #{error_text} is not: #{error_content}" if !error_text.eql?(error_content)
    end

    def check_terms_and_conditions_error_text
      error_content = @data_manager.content["joinTermsAndConditionsErrorText"]
      error_text = accessor.store_element_text('join.terms-and-conditions-error', conditions.displayed?)
      raise ElementNotFoundError, "The error text: #{error_text} is not: #{error_content}" if !error_text.eql?(error_content)
    end

    def check_mandatory_errors_text
      failures = []
      default_country_content = @data_manager.content["countryDefault"]
      default_language_content = @data_manager.content["languageDefault"]
      default_hours_flown_content = @data_manager.content["hoursFlownDefault"]

      email_error_content = @data_manager.content["joinRequiredEmailErrorText"]
      password_error_content = @data_manager.content["joinRequiredPasswordErrorText"]
      confirm_password_error_content = @data_manager.content["joinRequiredConfirmPasswordErrorText"]
      firstname_error_content = @data_manager.content["joinRequiredFirstNameErrorText"]
      lastname_error_content = @data_manager.content["joinRequiredLastNameErrorText"]
      country_error_content = @data_manager.content["joinRequiredCountryErrorText"]
      language_error_content = @data_manager.content["joinRequiredLanguageErrorText"]
      hours_flown_error_content = @data_manager.content["joinRequiredHoursFlownErrorText"]
      terms_and_conditions_error_content = @data_manager.content["joinTermsAndConditionsErrorText"]

      email_error_text = accessor.store_element_text("join.required-email-error", conditions.displayed?, :timeout => 1, :suppress_timeout_error => true)
      password_error_text = accessor.store_element_text("join.required-password-error", conditions.displayed?, :timeout => 1, :suppress_timeout_error => true)
      confirm_password_error_text = accessor.store_element_text("join.required-confirm-password-error", conditions.displayed?, :timeout => 1, :suppress_timeout_error => true)
      firstname_error_text = accessor.store_element_text("join.required-firstname-error", conditions.displayed?, :timeout => 1, :suppress_timeout_error => true)
      lastname_error_text = accessor.store_element_text("join.required-lastname-error", conditions.displayed?, :timeout => 1, :suppress_timeout_error => true)
      country_error_text = accessor.store_element_text("join.required-country-error", conditions.displayed?, :timeout => 1, :suppress_timeout_error => true)
      language_error_text = accessor.store_element_text("join.required-language-error", conditions.displayed?, :timeout => 1, :suppress_timeout_error => true)
      hours_flown_error_text = accessor.store_element_text("join.required-hours-flown-error", conditions.displayed?, :timeout => 1, :suppress_timeout_error => true)
      terms_and_conditions_error_text = accessor.store_element_text('join.terms-and-conditions-error', conditions.displayed?, :timeout => 1, :suppress_timeout_error => true)

      failures << "The required email error text: #{email_error_text} is not: #{email_error_content}." if !email_error_text.eql?(email_error_content) && email_text.blank?
      failures << "The required password error text: #{password_error_text} is not: #{password_error_content}." if !password_error_text.eql?(password_error_content) && password_text.blank?
      failures << "The required confirm password error text: #{confirm_password_error_text} is not: #{confirm_password_error_content}." if !confirm_password_error_text.eql?(confirm_password_error_content) && confirm_password_text.blank?
      failures << "The required firstname error text: #{firstname_error_text} is not: #{firstname_error_content}." if !firstname_error_text.eql?(firstname_error_content) && firstname_text.blank?
      failures << "The required lastname error text: #{lastname_error_text} is not: #{lastname_error_content}." if !lastname_error_text.eql?(lastname_error_content) && lastname_text.blank?
      failures << "The required country error text: #{country_error_text} is not: #{country_error_content}." if !country_error_text.eql?(country_error_content) && country_text.eql?(default_country_content)
      failures << "The required language error text: #{language_error_text} is not: #{language_error_content}." if !language_error_text.eql?(language_error_content) && language_text.eql?(default_language_content)
      failures << "The required hours flown error text: #{hours_flown_error_text} is not: #{hours_flown_error_content}." if !hours_flown_error_text.eql?(hours_flown_error_content) && hours_flown_text.eql?(default_hours_flown_content)
      failures << "The required terms and conditions error text: #{terms_and_conditions_error_text} is not: #{terms_and_conditions_error_content}." if !terms_and_conditions_error_text.eql?(terms_and_conditions_error_content) && !terms_and_conditions_checkbox_selected?
      failures
    end

    def check_verification_message
      #TODO Raise page error if the message does not match the expected content.
      failures = []
      verification_message_content = @data_manager.content["joinVerificationMessage"]
      failures << "The registration verification message does not contain #{verification_message_content}." if !verification_message_text.include?(verification_message_content)
      failures
    end

    def check_verification_email_content(message, role)
      failures = []
      case role
      when "member"
        verification_email_content = @data_manager.content["joinVerificationEmailMessage"]
        member_verification_email_content = @data_manager.content["joinMemberVerificationEmailMessage"]
        failures << "The member verification email does not contain #{verification_email_content}." if !message.include?(verification_email_content)
        failures << "The member verification email does not contain #{member_verification_email_content}." if !message.include?(member_verification_email_content)
      when "delegate"
        verification_email_content = @data_manager.content["joinVerificationEmailMessage"]
        delegate_verification_email_content = @data_manager.content["joinDelegateVerificationEmailMessage"]
        failures << "The delegate verification email does not contain #{verification_email_content}." if !message.include?(verification_email_content)
        failures << "The delegate verification email does not contain #{delegate_verification_email_content}." if !message.include?(delegate_verification_email_content)
      end
      failures
    end

    def check_confirmation_message(role)
      failures = []
      case role
      when "member"
        confirmation_message_text = accessor.store_element_text("join.registration-confirmation", conditions.displayed?)
        greeting_message_content = @data_manager.content["joinGreetingMessage"]
        member_confirmation_message_content = @data_manager.content["joinMemberConfirmationMessage"]
        failures << "The greeting message does not contain #{greeting_message_content}." if !confirmation_message_text.include?(greeting_message_content)
        failures << "The member registration confirmation message does not contain #{member_confirmation_message_content}." if !confirmation_message_text.include?(member_confirmation_message_content)
      when "delegate"
        #TODO Phantomjs is not reading the text of the h2 element.
        raise ElementNotFoundError, "The delegate popup was not displayed." unless wait_until_delegate_popup_opened?
        confirmation_message_text = accessor.store_element_text("join.delegate-popup.confirmation-message-info", conditions.displayed?)
        #greeting_message_content = @data_manager.content["joinGreetingMessage"]
        delegate_confirmation_message_content = @data_manager.content["joinDelegateConfirmationMessage"]
        #failures << "The greeting message does not contain #{greeting_message_content}." if !confirmation_message_text.include?(greeting_message_content)
        failures << "The delegate registration confirmation message does not contain #{delegate_confirmation_message_content}." if !confirmation_message_text.include?(delegate_confirmation_message_content)
      end
      failures
    end

    def check_confirmation_email_content(message)
      failures = []
      confirmation_email_content = @data_manager.content["joinConfirmationEmailMessage"]
      failures << "The confirmation email does not contain #{confirmation_email_content}." if !message.include?(confirmation_email_content)
      failures
    end

    def read_verification_email(user)
      verification_email_title = @data_manager.content["joinVerificationEmailTitle"]
      messages = GmailUtils.unread_emails(user.linked_email_address, user.linked_email_password, :to => user.email, :subject => verification_email_title)
      raise DataError, "A message could not be found that has subject: #{verification_email_title} and contains: #{user.firstname}." if messages.empty?
      Nokogiri::HTML(messages.first.to_s).text
    end

    def read_confirmation_email(user)
      confirmation_email_title = @data_manager.content["joinConfirmationEmailTitle"]
      messages = GmailUtils.unread_emails(user.linked_email_address, user.linked_email_password, :to => user.email, :subject => confirmation_email_title)
      raise DataError, "A message could not be found that has subject: #{confirmation_email_title} and contains: #{user.firstname}." if messages.empty?
      Nokogiri::HTML(messages.first.to_s).text
    end

    def verify_email_account(email_message)
      #TODO Check that the email message is not nil.
      matched_url = email_message.match(/#{@environment.property("flyvictor-url")}(.*)/)
      action.navigate_to(matched_url[0])
    end

    def wait_until_delegate_popup_opened?
      synchroniser.wait_until_element_located("join.delegate-popup", conditions.displayed?)
    end

    def wait_until_delegate_popup_closed?
      synchroniser.wait_until_element_located("join.delegate-popup", conditions.hidden?)
    end

    def wait_until_success_panel_opened?
      synchroniser.wait_until_element_located("join.delegate-popup.success-panel", conditions.displayed?)
    end

    def is_open?
      synchroniser.wait_until_element_located("join.registration-form", conditions.displayed?.text_contains?("Become a member"), :timeout => 1)
    end

    def wait_until_opened?
      synchroniser.wait_until_element_located("join.registration-form", conditions.displayed?.text_contains?("Become a member"))
    end

    private

    def verification_message_text
      accessor.store_element_text("join.confirmation-message-info", conditions.displayed?)
    end

    def join_email
      accessor.search_for_element("join.email", conditions.displayed?.enabled?)
    end

    def email_placeholder
      join_email.attribute("placeholder")
    end

    def email_text
      accessor.store_element_text("join.email", conditions.displayed?)
    end

    def join_password
      accessor.search_for_element("join.password", conditions.displayed?.enabled?)
    end

    def password_placeholder
      join_password.attribute("placeholder")
    end

    def password_text
      accessor.store_element_text("join.password", conditions.displayed?)
    end

    def join_confirm_password
      accessor.search_for_element("join.confirm-password", conditions.displayed?.enabled?)
    end

    def confirm_password_placeholder
      join_confirm_password.attribute("placeholder")
    end

    def confirm_password_text
      accessor.store_element_text("join.confirm-password", conditions.displayed?)
    end

    def join_phone
      accessor.search_for_element("join.phone", conditions.displayed?.enabled?)
    end

    def phone_number_placeholder
      join_phone.attribute("placeholder")
    end

    def phone_number_text
      accessor.store_element_text("join.phone", conditions.displayed?)
    end

    def join_title
      accessor.search_for_element("join.title", conditions.displayed?.enabled?)
    end

    def titles
      action.options_text(join_title)
    end

    def title_text
      action.first_selected_option_text(join_title)
    end

    def join_firstname
      accessor.search_for_element("join.firstname", conditions.displayed?.enabled?)
    end

    def firstname_placeholder
      join_firstname.attribute("placeholder")
    end

    def firstname_text
      accessor.store_element_text("join.firstname", conditions.displayed?)
    end

    def join_lastname
      accessor.search_for_element("join.lastname", conditions.displayed?.enabled?)
    end

    def lastname_placeholder
      join_lastname.attribute("placeholder")
    end

    def lastname_text
      accessor.store_element_text("join.lastname", conditions.displayed?)
    end

    def join_country
      accessor.search_for_element("join.country", conditions.displayed?.enabled?)
    end

    def countries
      action.options_text(join_country)
    end

    def country_text
      action.first_selected_option_text(join_country)
    end

    def join_language
      accessor.search_for_element("join.language", conditions.displayed?.enabled?)
    end

    def languages
      action.options_text(join_language)
    end

    def language_text
      action.first_selected_option_text(join_language)
    end

    def join_hours
      accessor.search_for_element("join.hours-flown", conditions.displayed?.enabled?)
    end

    def hours
      action.options_text(join_hours)
    end

    def hours_text
      action.first_selected_option_text(join_hours)
    end

    def terms_and_conditions_checkbox_selected?
      synchroniser.wait_until_element_located("join.terms-and-conditions-checkbox", conditions.selected?, :timeout => 1)
    end
  end
end
