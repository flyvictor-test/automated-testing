class DataError < StandardError
  def initialize(msg)
    super(msg)
  end
end
