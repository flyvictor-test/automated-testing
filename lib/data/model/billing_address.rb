class BillingAddress
  attr_reader :address1, :city, :country

  def initialize(address1, city, country)
    @address1 = address1
    @city = city
    @country = country
  end
end
