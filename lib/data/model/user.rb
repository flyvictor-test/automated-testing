class User
  attr_accessor :id, :legacy_id, :email, :password, :linked_email_address, :linked_email_password, :title, :firstname, :lastname, :phone_number, :country, :language, :hours_flown

  def initialize(email, password)
    @email = email
    @password = password
  end

  def self.from_json(json)
    data = JSON.parse(json)
    email = data['email'].nil? ? "fly.victortester+#{DateTime.now.strftime("%d%m%y%H%M%S")}@gmail.com" : data['email']
    user = User.new(email, data['password'])
    user.linked_email_address = data['linkedEmailAddress']
    user.linked_email_password = data['linkedEmailPassword']
    user.title = data['title']
    user.firstname = data['firstName']
    user.lastname = data['lastName']
    user.phone_number = data['phone']
    user.hours_flown = data['hoursFlown']
    user.country = data['country']
    user.language = data['languageCode']
    user
  end

  def to_json
    charter_frequency = hours_flown.split('-')
    { "users" => [{ :lastName => lastname,
                    :phone => phone_number,
                    :firstName => firstname,
                    :password => password,
                    :email => email,
                    :country => country,
                    :languageCode => language, :charterFrequency => { :min => charter_frequency[0].to_i, :max => charter_frequency[1].to_i },
                    :title => title
                  }]
    }.to_json
  end
end
