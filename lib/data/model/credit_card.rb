class CreditCard
  attr_reader :card_number, :expiry_month, :expiry_year, :verification_number, :secure_password

  def initialize(card_number, expiry_month, expiry_year, verification_number, secure_password)
    @card_number = card_number
    @expiry_month = expiry_month
    @expiry_year = expiry_year
    @verification_number = verification_number
    @secure_password = secure_password
  end
end
