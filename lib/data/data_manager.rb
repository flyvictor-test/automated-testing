require 'json'

class DataManager
  attr_reader :payload

  def initialize
    load_content
    load_users
    @billing_addresses = load_billing_addresses
    @credit_cards = load_credit_cards
  end

  def content(locale = "EN")
    @content[locale]
  end

  def user_by_role(role)
    json = JSON.generate(@users['users'][role])
    User.from_json(json)
  end

  def billing_address_by_user(username)
    @billing_addresses[username]
  end

  def credit_card_by_user(username)
    @credit_cards[username]
  end

  private

  def load_content
    @content = JSON.parse(File.read(json_file_path("content.json")).force_encoding("UTF-8"))
  end

  def load_users
    @users = JSON.parse(File.read(json_file_path("users.json")).force_encoding("UTF-8"))
  end

  def load_billing_addresses
    billing_addresses = Hash.new
    data = JSON.parse(File.read(json_file_path("users.json")))
    data['billingAddresses'].each { |name, billing_address|
      billing_addresses[name] = BillingAddress.new(billing_address['address1'], billing_address['city'], billing_address['country'])
    }
    billing_addresses
  end

  def load_credit_cards
    credit_cards = Hash.new
    data = JSON.parse(File.read(json_file_path("users.json")))
    data['creditCards'].each { |name, credit_card|
      credit_cards[name] = CreditCard.new(credit_card['cardNumber'], credit_card['expiryMonth'], credit_card['expiryYear'], credit_card['verificationNumber'], credit_card['3dSecurePassword'])
    }
    credit_cards
  end

  def json_file_path(filename)
    File.expand_path(File.join(File.dirname(__FILE__), "../../data/" + filename))
  end
end
