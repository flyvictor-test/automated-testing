class UnexpectedResponseError < StandardError
  def initialize(msg)
    super(msg)
  end
end