module Api
  class RoutingClientBase
    USER_AUTH_POST_URL = "user-authentication-tokens"
    USER_AUTH_GET_URL = "user-authentication-tokens/%s?userAuthToken=%s"
    USER_AUTH_DELETE_URL = "user-authentication-tokens/%s?userAuthToken=%s"

    def initialize(runtime)
      @environment = runtime.environment
      @logger = runtime.logger
    end

    def user_auth_post_url
      endpoint + USER_AUTH_POST_URL
    end

    def user_auth_get_url(user_token)
      endpoint + USER_AUTH_GET_URL % [user_token, user_token]
    end

    def user_auth_delete_url(user_token)
      endpoint + USER_AUTH_DELETE_URL % [user_token, user_token]
    end

    def endpoint
      @environment.property("routing-service-url")
    end

    def oauth_request
      Api::OauthRequest.new(@environment.property('routing-service-auth'), @environment.property('routing-service-secret'))
    end
  end
end