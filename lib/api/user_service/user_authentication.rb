module Api
  module UserService
    class UserAuthentication < Api::RoutingClientBase
      attr_reader :auth_response

      def initialize(runtime)
        super(runtime)
      end

      def create_auth_token(email, password)
        @auth_response = oauth_request.post(user_auth_post_url, :body => build_post_request(email, password), :headers => { "Content-Type" => "application/json" })
      end

      def read_auth_token
        begin
          return "" if JSON.parse(@auth_response.body)["user-authentication-tokens"].nil?
          return "" if JSON.parse(@auth_response.body)["user-authentication-tokens"][0].nil?
          JSON.parse(@auth_response.body)["user-authentication-tokens"][0]["token"]
        rescue JSON::ParserError
          ""
        end
      end

      def read_user_id
        begin
          return "" if JSON.parse(@auth_response.body)["user-authentication-tokens"].nil?
          return "" if JSON.parse(@auth_response.body)["user-authentication-tokens"][0].nil?
          JSON.parse(@auth_response.body)["user-authentication-tokens"][0]["links"]["user"]
        rescue JSON::ParserError
          ""
        end
      end

      def delete_auth_token(user_token)
        @auth_response = oauth_request.delete(user_auth_delete_url(user_token))
      end

      private

      def build_post_request(email, password)
        { "user-authentication-tokens" => [{ :username => email, :password => password }] }.to_json
      end
    end
  end
end