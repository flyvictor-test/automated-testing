class UnsupportedConfigurationError < StandardError
  def initialize(msg)
    super(msg)
  end
end